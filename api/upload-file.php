<?php
chdir("../");
require('./system/base/initial-load.php');
$currentUser = GetCurrentUser();
$allowed = array('png', 'jpg', 'JPG', 'mp4');
$imagesExts = array('png', 'jpg', 'JPG');
$videoExts = array('mp4');
$requestData = $_REQUEST;
$response = array();
$fieldName = 'fileAttachment2';
$FILES = array();
$FILES['name'] = $_FILES['fileAttachment2']['name']['0'];
$FILES['type'] = $_FILES['fileAttachment2']['type']['0'];
$FILES['tmp_name'] = $_FILES['fileAttachment2']['tmp_name']['0'];
$FILES['error'] = $_FILES['fileAttachment2']['error']['0'];
$FILES['size'] = $_FILES['fileAttachment2']['size']['0'];

if(isset($FILES) && $FILES['error'] == 0)
{
  $extension = pathinfo($FILES['name'], PATHINFO_EXTENSION);
  if(!in_array(strtolower($extension), $allowed))
  {
    $message = 'File extension not allowed';
    SendErrorResponse($message);
  }
  
  if(move_uploaded_file($FILES['tmp_name'], TMP_UPLOADED_FILES_PATH . $FILES['name']))
  {
    $response['tmpUploadedFile'] = GenerateImageAbsolutePath(TMP_UPLOADED_FILES_PATH . $FILES['name']);
    $fileType = '';
    if (in_array($extension, $imagesExts))
      $fileType = 'image';
    else if (in_array($extension, $videoExts))
      $fileType = 'video';
    $response['fileType'] = $fileType;
    
    
    SendResponse($response);
  }
} 
else
  SendErrorResponse('Photo not found');