<?php
chdir("../");
require('./system/base/initial-load.php');
$currentUser = GetCurrentUser();
if (!ValidId($currentUser->id))
  SendErrorResponse ('user-not-logged-in');
$requestData = $_POST;

if (!isset($requestData['thumbUrl']) || strlen($requestData['thumbUrl']) < 1)
  $requestData['thumbUrl'] = null;
if (!isset($requestData['videoUrl']) || strlen($requestData['videoUrl']) < 1)
  SendErrorResponse("Please select a video");
if (!isset($requestData['title']) || strlen($requestData['title']) < 1)
  SendErrorResponse("Please write a title");
if (!isset($requestData['category']) || strlen($requestData['category']) < 1)
  SendErrorResponse("Please select a category");

$video = new Video();
$video->title = $requestData['title'];
$video->description = $requestData['description'];
$video->category = $requestData['category'];
$video->status = Video::STATUS_PUBLIC;
$video->userId = $currentUser->id;
$video->dateUploaded = FormatSqlDatetime(time());

$videoFileDst = CHANNEL_FILES_PATH . $currentUser->id . '/' . pathinfo($requestData['videoUrl'], PATHINFO_FILENAME) . rand(0, 200000) . "." . pathinfo($requestData['videoUrl'], PATHINFO_EXTENSION);
$thumbFileDst = CHANNEL_FILES_PATH . $currentUser->id . '/' . basename($requestData['thumbUrl']);
if (!file_exists(CHANNEL_FILES_PATH . $currentUser->id . '/'))
  mkdir(CHANNEL_FILES_PATH . $currentUser->id . '/', 0775, true);

if (rename(TMP_UPLOADED_FILES_PATH . basename($requestData['videoUrl']), $videoFileDst))
{
  if (!file_exists(getcwd() . '/' . $videoFileDst))
    SendErrorResponse ("file-not-found");
  $video->videoUrl = $videoFileDst;
}
if ($requestData['thumbUrl'])
  if (rename(TMP_UPLOADED_FILES_PATH . basename($requestData['thumbUrl']), $thumbFileDst))
  {
    $video->thumbUrl = $thumbFileDst;
  }

$video->Save();
if (ValidId($video->id))
{
  $video->videoThumbUrl = GetScreenshotFromVideo(getcwd() . "/" . $video->videoUrl, pathinfo($video->videoUrl, PATHINFO_DIRNAME) . "/" . pathinfo($video->videoUrl, PATHINFO_FILENAME) . '.jpg');
  $video->Save();
}

$response = array(
  'redirectUrl' => SITE_URL . GetControllerUrl('video', $video->id)
);
SendResponse($response);
//$video->videoUrl = $requestData[];
//$video->thumbUrl = $requestData[];
//$video->videoThumbUrl = $requestData[];

function GetScreenshotFromVideo($videoUrl, $outputUrl)
{
  $outputJpg = getcwd() . "/" . $outputUrl;
  $outputTxt = pathinfo($videoUrl, PATHINFO_DIRNAME) . "/output.txt";
  exec(FFMPEG_COMMAND . " -ss 00:00:01 -i \"$videoUrl\" -vframes 1 -q:v 2 \"$outputJpg\" 2> \"$outputTxt\"");
  if (file_exists($outputJpg))
  {
    return $outputUrl;
  }
  
  return null;
}