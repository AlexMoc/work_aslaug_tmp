<?php
chdir("../../");
require('./system/base/initial-load.php');

$requestData = $_POST;

$response = array();

$user = GetCurrentUser();

$session = new Session($user);

$session->LogoutUser();
$cookies = array();
$cookies[COOKIE_NAME] = NULL;
$response['cookies'] = $cookies;
$response['reload'] = true;
SendResponse($response);

?>