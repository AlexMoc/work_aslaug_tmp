<?php
chdir("../../");
require('./system/base/initial-load.php');

$requestData = $_POST;

$response = array();

if (preg_match('/([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/', $requestData['email']) != 1)
  SendErrorResponse('email not matched');

if (!$requestData['password'])
  SendErrorResponse("Password Required");

if (!$requestData['yourName'])
  SendErrorResponse("Name Required");
if (!$requestData['userName'])
  SendErrorResponse("Username Required");
if (!$requestData['phoneNumber'])
  SendErrorResponse("Phone Number Required");
if (!$requestData['agreeTerms'])
  SendErrorResponse("You must agree terms");

$user = User::LoadUserByEmail($requestData['email']);
if (!ValidId($user->id))
{
  CreateNewUser($user, $requestData);
}
else
  SendErrorResponse('Existing mail');
  
$cookies[COOKIE_NAME] = LogInUserAndReload($user);
$response['cookies'] = $cookies;
SendResponse($response);

/**
 * 
 * @param User $user
 * @param type $data
 */
function CreateNewUser($user, $data)
{
  $user->name = $data['yourName'];
  $user->userName = $data['userName'];
  $user->email = $data['email'];
  $user->phoneNumber = $data['phoneNumber'];
  $user->password = md5($data['password']);
  $user->type = User::TYPE_USER;
  $user->termsAccepted = $data['agreeTerms'];
  $user->newsletterSubscribe = $data['newsletterSubscribe'];
  $user->date = FormatSqlDatetime(time());
  $user->dateUpdated = FormatSqlDatetime(time());
  
  $user->Save();
}
?>