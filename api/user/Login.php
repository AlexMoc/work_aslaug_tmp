<?php
chdir("../../");
require('./system/base/initial-load.php');

$requestData = $_POST;

$response = array();

$user = UserExist($requestData['email'], md5($requestData['password']));

if (ValidId($user->id))
{
  $cookies = array();
  $cookies[COOKIE_NAME] = LogInUserAndReload($user);
  $response['cookies'] = $cookies;
  $response['redirectUrl'] = SITE_URL . GetControllerUrl('channel', $user->id);
  SendResponse($response);
}
else
{
  $response['error'] = 'Invalid nick or password';
  SendErrorResponse($response);
}

?>