<?php
chdir("../../");
require('./system/base/initial-load.php');

$requestData = $_POST;
$action = $_GET['action'];
$response = array();

$currentUser = GetCurrentUser();
if (!ValidId($currentUser->id))
  SendErrorResponse("User not logged in");
$isVideo = false;
$isComment = false;
if (isset($requestData['videoId']))
{
  $isVideo = true;
  $entity = new Video($requestData['videoId']);
  $entityType = Like::ENTITY_VIDEO;
}
else if (isset($requestData['commentId']))
{
  $isComment = true;
  $entity = new VideoActivity($requestData['commentId']);
  $entityType = Like::ENTITY_COMMENT;
}
else
  SendErrorResponse ("Entity not found");

$prevLike = Like::LoadPrevEntityLike($currentUser, $entity);

if ($action == 'like-video')
  $likeCategory = Like::CATEGORY_LIKE;
else if ($action == 'dislike-video')
  $likeCategory = Like::CATEGORY_DISLIKE;
else if ($action == 'like-comment')
  $likeCategory = Like::CATEGORY_LIKE;
else if ($action == 'dislike-comment')
  $likeCategory = Like::CATEGORY_DISLIKE;
else
  SendResponse ('Invalid action');

$newLike = new Like();
$newLike->userId = $currentUser->id;
$newLike->likedEntityId = $entity->id;
$newLike->likedType = $entityType;
$newLike->likeCategory = $likeCategory;
$newLike->date = FormatSqlDatetime(time());

if (ValidId($prevLike->id))
{
  if ($prevLike->likeCategory == $newLike->likeCategory)
    $prevLike->Delete();
  else
  {
    $prevLike->Delete();
    $newLike->Save();
  }
}
else
{
  $newLike->Save();
}

if ($isVideo)
{
  require_once(TOOLS_PATH . 'video-stats.inc.php');
  UpdateVideoStats($entity);
  $entityStats = $entity->GetStats();
}
else if ($isComment)
{
  require_once(TOOLS_PATH . 'video-stats.inc.php');
  UpdateCommentStats($entity);
  $entityStats = $entity;
}


$response = array(
  'likesCount' => $entityStats->likesCount,
  'dislikesCount' => $entityStats->dislikesCount,
);

SendResponse($response);

?>