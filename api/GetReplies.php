<?php
chdir("../");
require('./system/base/initial-load.php');
$currentUser = GetCurrentUser();
$response = array();
$requestData = $_POST;

if (isset($requestData['videoId']))
  $video = new Video($requestData['videoId']);

if (!isset($requestData['commentId']))
  SendErrorResponse ("Invalid comment");

$where = 'va.video_id=' . ToSqlQuotedString($video->id)
       . ' AND va.status=' . ToSqlQuotedString(VideoActivity::STATUS_NEW)
       . ' AND va.parent_id=' . ToSqlQuotedString($requestData['commentId']);
$videoActivityReplies = VideoActivity::LoadVideoActivities($where, null, "ORDER BY id DESC");
array_shift($videoActivityReplies);
array_shift($videoActivityReplies);
$html = '';
require_once(TOOLS_PATH . 'video-activities.inc.php');
foreach ($videoActivityReplies as $reply)
{
  $html .= RenderReplyActivity($reply);
}
$response['activitiesHtml'] = $html;

SendResponse($response);