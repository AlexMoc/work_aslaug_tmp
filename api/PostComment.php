<?php
chdir("../");
require('./system/base/initial-load.php');
$currentUser = GetCurrentUser();
$response = array();
if (!ValidId($currentUser->id))
  SendErrorResponse ('user-not-logged-in');
$requestData = $_POST;

$video = new Video($requestData['videoId']);
if (!ValidId($video->id))
  SendErrorResponse ('invalid-video');

if (!isset($requestData['comment']))
  SendErrorResponse ('Please insert text');
if (strlen($requestData['comment']) < 1)
  SendErrorResponse ('Please insert text');

$videoActivity = new VideoActivity();
$videoActivity->comment = $requestData['comment'];
$videoActivity->userId = $currentUser->id;
$videoActivity->videoId = $video->id;
$videoActivity->date = FormatSqlDatetime(time());
$videoActivity->status = VideoActivity::STATUS_NEW;

if (isset($requestData['commentId']) && ValidId($requestData['commentId']))
  $videoActivity->parentId = $requestData['commentId'];

$videoActivity->Save();

if (ValidId($videoActivity->id))
{
  require_once(TOOLS_PATH . 'video-activities.inc.php');
  if (ValidId($videoActivity->parentId))
  {
    $alias = 'replyActivity';
    $activityContent = RenderReplyActivity($videoActivity);
  }
  else
  {
    $alias = 'videoActivity';
    $activityContent = RenderVideoActivity($videoActivity);
  }
  $response = array(
    $alias => $activityContent
  );
  SendResponse($response);
}