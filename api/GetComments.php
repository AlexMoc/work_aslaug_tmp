<?php
chdir("../");
require('./system/base/initial-load.php');
$currentUser = GetCurrentUser();
$response = array();
$requestData = $_POST;

if (isset($requestData['videoId']))
  $video = new Video($requestData['videoId']);

$paginationData = array(
  'from' => $requestData['from'],
  'length' => VIDEO_ACTIVITIES_FEED_SIZE
);
$where = 'va.video_id=' . ToSqlQuotedString($video->id)
       . ' AND va.status=' . ToSqlQuotedString(VideoActivity::STATUS_NEW)
       . ' AND va.parent_id IS NULL';
$videoActivities = VideoActivity::LoadVideoActivities($where, null, "ORDER BY id DESC", $paginationData);

$html = '';
require_once(TOOLS_PATH . 'video-activities.inc.php');
foreach ($videoActivities as $videoActivity)
{
  $html .= RenderVideoActivity($videoActivity, TRUE);
}
$response['activitiesHtml'] = $html;
$response['hasMore'] = $paginationData['total'] > ($paginationData['from'] + $paginationData['length']);

SendResponse($response);