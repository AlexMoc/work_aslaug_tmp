function ModalForm (id) {
  var form = {
    formContainer: $("#form-container-overlay"),
    id: id,
    el: $('#form-container-' + id),
    errorContainer: $('#form-container-' + id + ' .error-container'),
    fileUpload:
    {
      haveFileUpload: false,
      data: undefined,
      ajaxData: 
      {
        url: '',
        pars: [],
        fnSuccess: null,
        fnError: null
      }
    },
    getObject: function () 
    {
      return this.el;
    },
    getFormUploadImageElementWrapper: function ()
    {
      var $container = this.getObject();
      var $el = $container.find(".file-upload-wrapper");
      return $el;
    },
    show: function ()
    {
      this.formContainer.show();
      this.el.show();
      this.hideLoadingProgress();
      var $jqFileInputs = $("body").find('input:file').filter('.jquery-file-upload');
      var _this = this;
      $jqFileInputs.each(function(key, el)
      {
        _this.fileUpload.haveFileUpload = true;
        var $jqFileInput = $(el);
        // file upload support
        $jqFileInput.fileupload(
        {
          dataType: 'json',
          maxFileSize: 10 * 1024 * 1024, // 10MB max,
          acceptFileTypes: /(\.|\/)(gif|jpe?g|png|wmv|mp4|mov|mpg)$/i,
          redirect: '/asdsad',
          paramName: 'files',
          singleFileUploads: false,
          url: window.SITE_URL + '/api/upload-file.php',
          add: function(e, data)
          {
            _this.fileUpload.data = data;
            var $el = _this.getFormUploadImageElementWrapper();
            $.each(data.files, function (index, file) {
              $el.append('<div class="uploaded-item">' + file.name + '</div>');
            });
//          var jqXHR = data.submit()
//              .success(function (result, textStatus, jqXHR) {/* ... */})
//              .error(function (jqXHR, textStatus, errorThrown) {/* ... */})
//              .complete(function (result, textStatus, jqXHR) {/* ... */});
          },
          fail: function(e, data)
          {
            console.info('file upload fail', e, data);
          },
          error: function(e, data)
          {
            console.info('file upload error', e, data);
          },
          success: function(response, data)
          {
            jQuery.extend(_this.fileUpload.ajaxData.pars, response);
            AjaxRequest(_this.fileUpload.ajaxData.url, _this.fileUpload.ajaxData.pars, _this.fileUpload.ajaxData.fnSuccess, _this.fileUpload.ajaxData.fnError);
          }
        });
      }); 
    },
    close: function ()
    {
      this.formContainer.hide();
      this.el.hide();
    },
    setError: function (value)
    {
      this.errorContainer.html(value);
      this.errorContainer.show();
    },
    collectFormParams: function()
    {
      var formValues = {};
      $('#' + id).find('input.text-box').each(function(index, el){
        var $el = $(el);
        formValues[$el.attr('name')] = $el.val();
      });
      
      $('#' + id).find('select').each(function(index, el){
        var $elPrim = $(el);
        var $el = $(el).find(":selected");
        formValues[$elPrim.attr('name')] = $el.val();
      });
      
      var checkBoxWrapper = $('#' + id).find('.check-box-options');
      var name = checkBoxWrapper.attr('name');
      if (name)
        formValues[name] = {};
      checkBoxWrapper .find('input:checked').each(function(index, el){
        var $el = $(el);
        formValues[name][$el.val()] = 1;
      });
      
      var checkableBoxWrapper = $('#' + id).find('.checkable-elements-wrapper').each(function(index, el){
        var name = $(el).attr('id');
        if (name)
          formValues[name] = {};
        var selectedElements = $(el).find(".selected").each(function(index, el){
          formValues[name][index] = $(el).attr('value');
        });
      });
      
      if ($('#' + id).find('.photos-preview'))
      {
        var name = 'uploadedPhotos';
        formValues[name] = {};
        var uploadedPhotos = $('#' + id).find('.photos-preview').each(function(index, el){
          var value = $(el).data('url');
          if (value)
            formValues[name][index] = value;
        });
      }
      
      
      
      return formValues;
    },
    showLoadingProgress: function()
    {
      $(".loading-overlay").show();
    },
    hideLoadingProgress: function()
    {
      $(".loading-overlay").hide();
    }
  }
  $( document ).keyup(function(e) {

    if (e.keyCode == 27) 
    {
      form.close();
    }
  });
  return form;
}


function ClassicForm (id) {
  var form = {
    id: id,
    el: $('#form-container-' + id),
    errorContainer: $('#form-container-' + id + ' .error-container'),
    fileUpload:
    {
      haveFileUpload: false,
      data: undefined,
      ajaxData: 
      {
        url: '',
        pars: [],
        fnSuccess: null,
        fnError: null
      }
    },
    getObject: function () 
    {
      return this.el;
    },
    getFormUploadImageElementWrapper: function ()
    {
      var $container = this.getObject();
      var $el = $container.find(".file-upload-wrapper");
      return $el;
    },
    init: function ()
    {
      var $jqFileInputs = $("body").find('input:file').filter('.jquery-file-upload');
      var _this = this;
      $jqFileInputs.each(function(key, el)
      {
        _this.fileUpload.haveFileUpload = true;
        var $jqFileInput = $(el);
        // file upload support
        $jqFileInput.fileupload(
        {
          dataType: 'json',
          maxFileSize: 50 * 1024 * 1024, // 50MB max,
          acceptFileTypes: /(\.|\/)(gif|jpe?g|png|wmv|mp4|mov|mpg)$/i,
          redirect: '/asdsad',
          paramName: 'files',
          singleFileUploads: false,
          url: window.SITE_URL + '/api/upload-file.php',
          add: function(e, data)
          {
            _this.fileUpload.data = data;
            var $el = _this.getFormUploadImageElementWrapper();
            $.each(data.files, function (index, file) {
              var class1 = "";
              if (file.type == "video/mp4")
                class1 = "fa-video-camera";
              
              var html = '';
              html = '<div class="uploaded-item">'
                    + '<span class="file-type ' + file.type.replace("/", "-") + '"><i class="fa ' + class1 + '"></i> </span>'
                    + '<span class="file-name">' + file.name + '</span>'
                    + '<div class="upload-progress">' 
                        + '<div class="progress-bar-container">'

                            + '<div class="progress-bar-value"></div>'

                        + '</div>'
                    + '</div>'
                    + '<span class="convert-progress"></span>';
              
              html += '</div>';
              $el.append(html);
            });
//          var jqXHR = data.submit()
//              .success(function (result, textStatus, jqXHR) {/* ... */})
//              .error(function (jqXHR, textStatus, errorThrown) {/* ... */})
//              .complete(function (result, textStatus, jqXHR) {/* ... */});
          },
          fail: function(e, data)
          {
            console.info('file upload fail', e, data);
          },
          error: function(e, data)
          {
            console.info('file upload error', e, data);
          },
          success: function(response, data)
          {
            jQuery.extend(_this.fileUpload.ajaxData.pars, response);
            AjaxRequest(_this.fileUpload.ajaxData.url, _this.fileUpload.ajaxData.pars, _this.fileUpload.ajaxData.fnSuccess, _this.fileUpload.ajaxData.fnError);
          }
          
        }).bind("fileuploadprogress", function (e, data){console.info(e);console.info(data);});
      }); 
    },
    close: function ()
    {
      this.formContainer.hide();
      this.el.hide();
    },
    setError: function (value)
    {
      this.errorContainer.html(value);
      this.errorContainer.show();
    },
    collectFormParams: function()
    {
      var formValues = {};
      $('#' + id).find('input.text-box').each(function(index, el){
        var $el = $(el);
        formValues[$el.attr('name')] = $el.val();
      });
      
      $('#' + id).find('select').each(function(index, el){
        var $elPrim = $(el);
        var $el = $(el).find(":selected");
        formValues[$elPrim.attr('name')] = $el.val();
      });
      
      var checkBoxWrapper = $('#' + id).find('.checkbox-wrapper');
      checkBoxWrapper .find('input:checked').each(function(index, el){
        var $el = $(el);
        var name = $el.attr('name');
        formValues[name] = 1;
      });
      
      var checkableBoxWrapper = $('#' + id).find('.checkable-elements-wrapper').each(function(index, el){
        var name = $(el).attr('id');
        if (name)
          formValues[name] = {};
        var selectedElements = $(el).find(".selected").each(function(index, el){
          formValues[name][index] = $(el).attr('value');
        });
      });
      
      if ($('#' + id).find('.photos-preview'))
      {
        var name = 'uploadedPhotos';
        formValues[name] = {};
        var uploadedPhotos = $('#' + id).find('.photos-preview').each(function(index, el){
          var value = $(el).data('url');
          if (value)
            formValues[name][index] = value;
        });
      }
      
      
      
      return formValues;
    },
    showLoadingProgress: function()
    {
      $(".loading-overlay").show();
    },
    hideLoadingProgress: function()
    {
      $(".loading-overlay").hide();
    }
  }
  $( document ).keyup(function(e) {

    if (e.keyCode == 27) 
    {
      form.close();
    }
  });
  return form;
}

/**
 * AJAX request.
 * @param {string} url
 * @param {array} pars
 * @param {function} success
 * @param {runction} error
 * @returns {undefined}
 */
function AjaxRequest(url, pars, success, error)
{
  $.ajax({
    type: 'POST',
    url: url,
    data : pars,
    dataType: 'json',
    timeout: 300000,
    jsonp: 'jsonp_callback',
    xhrFields: {
      withCredentials: true
    },
    success: function(data)
    {
      var code = data.code;
      var message = data.message;
      switch(code)
      {
        case 200:
          if (data.cookies)
          {
            var cookies = data.cookies;
            for (var index in cookies)
            {
              $.cookie(index, cookies[index], {path    : '/', secure  : false});
            }
          }
          if (typeof(success) === "function")
            success(data);
          if (data.redirectUrl)
            window.location = data.redirectUrl;
          if (data.reload)
            location.reload();
          break;
        case 400:
          if (typeof(error) === "function")
            error(message);
          break;
        default:
          break;
      }
    }
  });
}


function AdminDataTable(id, url)
{
  return $('#' + id).DataTable( {
    pagination: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: url,
      type: 'POST'
    },
    fnServerData: function (sSource, aoData, fnCallback) {
      var dataTablesPars = {};
      for (var i = 0; i < aoData.length; i++)
        dataTablesPars[aoData[i].name] = aoData[i].value;

      var el = $(this).find("thead th")[dataTablesPars.order[0].column];
      var pars = {
        from :  dataTablesPars.start,
        length: dataTablesPars.length,
        sortColumn: $(el).attr("sname"),
        sortDir: dataTablesPars.order[0].dir
      };
      AjaxRequest(url, pars, function(data)
        {
          var total = data.total;
          if (total === 'unknown')
            total = 1000000;
          data.iTotalDisplayRecords = total;
          data.iTotalRecords = total;
          fnCallback(data);
        }, function(){console.info("ajax request error");});
    },
    error: function(){
      console.info("data table error");
    }
  } );
}