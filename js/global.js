function LogoutUser()
{
  AjaxRequest('/api/user/Logout.php');
}

function LikeVideo(el, videoId)
{
  var pars = {
    videoId: videoId
  };
  AjaxRequest('/api/video/VideoLike.php?action=like-video', pars, function(response){
    $(el).closest("ul").find(".likes-count").html(response.likesCount);
    $(el).closest("ul").find(".dislikes-count").html(response.dislikesCount);
  },
  function(response){
    alert(response.error);
  });
} 

function DislikeVideo(el, videoId)
{
  var pars = {
    videoId: videoId
  };
  AjaxRequest('/api/video/VideoLike.php?action=dislike-video', pars, function(response){
    $(el).closest("ul").find(".likes-count").html(response.likesCount);
    $(el).closest("ul").find(".dislikes-count").html(response.dislikesCount);
  },
  function(response){
    alert(response.error);
  });
}

function LikeComment(el, commentId)
{
  var pars = {
    commentId: commentId
  };
  AjaxRequest('/api/video/VideoLike.php?action=like-comment', pars, function(response){
    $(el).closest("ul").find(".likes-count").html(response.likesCount);
    $(el).closest("ul").find(".dislikes-count").html(response.dislikesCount);
  },
  function(response){
    alert(response.error);
  });
}

function DislikeComment(el, commentId)
{
  var pars = {
    commentId: commentId
  };
  AjaxRequest('/api/video/VideoLike.php?action=dislike-comment', pars, function(response){
    $(el).closest("ul").find(".likes-count").html(response.likesCount);
    $(el).closest("ul").find(".dislikes-count").html(response.dislikesCount);
  },
  function(response){
    alert(response.error);
  });
}

function AddReplyForm(el, commentId)
{
//  var activityWrap = $(el).closest("div.va-board");
  var activityWrap = $(el).closest("div.media-body");
  var formId = 'video-activity-reply-' + commentId;
  var $replyForm = $("#" + formId);
  
  if (!$replyForm.get(0))
    activityWrap.append('<form id="' + formId + '"><input type="text" class="textbox" /><input type="submit"/></form>');
  
  $replyForm = $("#" + formId);
  $replyForm.on("submit", function(e)
  {
    e.preventDefault();
    var pars = {
      commentId: commentId,
      comment: $replyForm.find("input.textbox").val(),
      videoId: window.currentVideo.id
    };
    AjaxRequest('/api/PostComment.php', pars, 
    function(response){
      var activityWrap = $(el).closest("div.va-board").next(".activitity-replies-wrapper").find(".activity-replies-content");
//      activityWrap.after(response.replyActivity);
      $(response.replyActivity).hide().prependTo(activityWrap).fadeIn("slow");
      $replyForm.find("input.textbox").val("");
    }, 
    function(response){
      alert(response.error);
    });
  })
}