VideoActivityPage = {
  from: 10,
  length: 10,
  $activitiesWrapper: $("#viedo-activities"),
  
  loadMoreActivities: function(el)
  {
    var _this = this;
    this.$activitiesWrapper = $("#viedo-activities");
    var pars = {
      from: this.from,
      videoId: window.currentVideo.id
    };
    AjaxRequest('/api/GetComments.php', pars, function(response){
      _this.$activitiesWrapper.append(response.activitiesHtml);
      _this.from += _this.length;
      if (!response.hasMore)
        $(el).hide();
    },
    function(response) {
      alert(response.error);
    });
  },
  
  loadAllreplies: function(el)
  {
    var $el = $(el);
    var pars = {
      commentId: $el.closest(".activitity-replies-wrapper").data('activity-id'),
      videoId: window.currentVideo.id
    };
    AjaxRequest('/api/GetReplies.php', pars, function(response){
      var $repliesWrapper = $el.closest(".activitity-replies-wrapper").find(".activity-replies-content");
      $repliesWrapper.append(response.activitiesHtml);
      $el.hide();
    },
    function(response) {
      alert(response.error);
    });
  }
  
}