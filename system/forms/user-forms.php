<?php
class AddMovie extends FormModal
{
  public function Init()
  {
    $this->BuildInputElement('movie-title', 'Movie Title', 'movie-title');
    $this->BuildInputElement('movie-genre', 'Movie Genre', 'movie-genre');
  }
};

class CreateAccount extends FormModal
{
  public function Init()
  {
    $this->BuildInputElement('email', 'Email', 'user-email');
    $this->BuildInputElement('nick', 'Nick', 'user-nick');
    $this->BuildPasswordElement('password', 'Password', 'user-password');
    $this->BuildPasswordElement('confirm-password', 'Confirm Password', 'confirm-user-password');
    $this->BuildSubmitButton('submit', 'Submit', 'create-account-submit');
  }
}

class UploadFile extends FormModal
{
  public function Init()
  {
//    $this->BuildInputElement('email', 'Email', 'user-email');
    $this->BuildUploadImageElement('file', 'Upload File', 'upload-file');
    
    $this->BuildSubmitButton('submit', 'Submit', 'create-account-submit');
  }
}

class HomeUploadFile extends ClassicForm
{
  public function Init()
  {
//    $this->BuildInputElement('email', 'Email', 'user-email');
    $this->BuildUploadImageElement('file', 'Upload File', 'upload-file');
    $this->BuildCheckboxElement('ccheck', "Check Here", 'check-id');
    
    $this->BuildSubmitButton('submit', 'Submit', 'create-account-submit');
  }
}

class SignUpForm extends ClassicForm
{
  public function Init()
  {
    $this->BuildInputElement('yourName', 'Your name', 'yourName', 'icon-communication-128');
    $this->BuildInputElement('userName', 'Username', 'userName', 'icon-finance-067');
    $this->BuildEmailElement('email', 'Your Email', 'email');
    $this->BuildPhoneNumberElement('phoneNumber', 'Your Phone Number', 'phoneNumber');
    $this->BuildPasswordElement('password', 'Password', 'password');
    $this->BuildCheckboxElement('agreeTerms', 'I accept the <a href="#" onclick="return false;">Terms and Conditions</a>', 'agreeTerms');
    $this->BuildCheckboxElement('newsletterSubscribe', 'Subscribe to our newsletter', 'newsletterSubscribe');
    
    $this->BuildSubmitButton('submit', 'Signup', 'create-account-submit');
    $this->BuildCustomHmlElement('loginText', '<p class="g-font-size-13 text-center mb-0">Already have an account? <a class="g-font-weight-600" href="' . GetControllerUrl('login') . '">Login</a></p>', 'loginText');
  }
}

class LoginForm extends ClassicForm
{
  public function Init()
  {
    $this->BuildEmailElement('email', 'Your Email', 'email');
    $this->BuildPasswordElement('password', 'Password', 'password');
    $this->BuildCheckboxElement('keepSignedIn', 'Keep signed in', 'keepSignedIn');
    
    $this->BuildSubmitButton('submit', 'Signup', 'create-account-submit');
    $this->BuildCustomHmlElement('loginText', '<p class="g-font-size-13 text-center mb-0">Don\'t have an account? <a class="g-font-weight-600" href="' . GetControllerUrl("signup") . '">signup</a></p>', 'loginText');

  }
}

?>