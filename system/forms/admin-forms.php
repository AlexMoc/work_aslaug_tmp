<?php

class AddMovieImage extends FormModal
{
  public function Init()
  {
    $this->BuildUploadImageElement('photo', 'Upload Image', 'upload-image');
    
    $this->BuildSubmitButton('submit-image', 'Submit', 'upload-image');
  }
}