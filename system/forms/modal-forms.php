<?php
class Element
{
  public $id;
  public $type;
  public $name;
  public $value;
  public $title;
  public $class;
  public $icon;
  public $isRequired;
  
  protected $validators = array();


  const TYPE_INPUT = 'text';
  const TYPE_PASSWORD = 'password';
  const TYPE_SUBMIT = 'submit';
  const TYPE_FILE = 'file';
  const TYPE_CHECKBOX = 'checkbox';
  const TYPE_EMAIL = 'email';
  const TYPE_TEL = 'tel';
  function __construct() 
  {
    
  }
  
  public function Render()
  {
    $html = '';
    if (strlen($this->title) > 0)
      $html .= '<label for="' . $this->name . '">' . $this->title . '</label>';
    $html .= '<input id="' . $this->id . '" class="' . $this->class . '" type="' . $this->type . '" data-url="http://google.com" name="' . $this->name . '" value="' . $this->value . '" />';
    return $html;
  }
};
class CustomHtmlElement extends Element
{
  public function Render()
  {
    $html = $this->title;
    return $html;
  }
}
class SubmitElement extends Element
{
  public function Render()
  {
    $html = '<div class="g-mb-30">'
          . '<input id="' . $this->id . '" class="btn rounded u-btn-primary btn-block text-uppercase g-py-13 ' . $this->class . '" type="' . $this->type . '" name="' . $this->name . '" value="' . $this->value . '"  />'
          . '</div>';
    return $html;
  }
}
class CheckBoxElement extends Element
{
  public function Render()
  {
    $html = '<div class="mb-1">'
            . '<label class="checkbox-wrapper form-check-inline u-check g-font-size-13 g-pl-25 mb-2" name="' . $this->name . '">'
              . '<input id="' . $this->id . '" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0 ' . $this->class . '" type="' . $this->type . '" name="' . $this->name . '" value="' . $this->value . '"  />'
              . '<div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">'
                . '<i class="fa g-rounded-2" data-check-icon="&#xf00c"></i>'
              . '</div>'
              . $this->title
            . '</label>'
          . '</div>';
    return $html;
  }
}
class InputElement extends Element
{
  public function Render()
  {
    $html = '<div class="mb-4">'
            . '<div class="input-group">'
              . '<span class="input-group-addon g-width-45 g-brd-gray-light-v4 g-color-primary">'
                . '<i class="' . $this->icon . ' u-line-icon-pro g-pos-rel g-top-2 g-px-5"></i>'
              . '</span>'
              . '<input id="' . $this->id . '" class="form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-15 ' . $this->class . '" type="' . $this->type . '" name="' . $this->name . '" value="' . $this->value . '" placeholder="' . $this->title . '">'
            . '</div>'
          . '</div>';
    return $html;
  }
}
class UploadImageElement extends Element
{
  public function Render()
  {
//    $html = '<div id="drop">'
//      . 'Drop Here'
//      . '<a>Browse</a>'
//      . '<input id="' . $this->id . '" class="' . $this->class . '" type="' . $this->type . '" name="' . $this->name . '" value="' . $this->value . '" multiple />'
//    . '</div>'
//    . '<ul>'
//    . '  <!-- The file uploads will be shown here -->'
//    . '</ul>';
    $html = '';
    if (strlen($this->title) > 0)
      $html .= '<label for="' . $this->name . '">' . $this->title . '</label>';
    $html .= '<div class="file-upload-wrapper">'
            . '<input id="' . $this->id . '" class="' . $this->class . '" type="' . $this->type . '"  name="' . $this->name . '" value="' . $this->value . '" multiple />'
           . '</div>';
    return $html;
  }
}
class FormModal
{
  /**@var string*/
  public $url;
  /**@var string*/
  public $title;
  /**@var Array<Element>*/
  public $elements;
  /**@var string*/
  public $formId;
  /**@var string*/
  public $formClass;
  /**@var Boolean*/
  public $isModal;
  
  function __construct($id = null, $title = null, $class = null) 
  {
    if (strlen($id) > 0)
      $this->formId = $id;
    if (strlen($title) > 0)
      $this->title = $title;
    if (strlen($class) > 0)
      $this->formClass = $class;
    $this->Init();
  }
  
  public function Init(){}
  
  public function BuildUploadImageElement($name, $title, $id)
  {
    $element = new UploadImageElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = '';
    $element->title = $title;
    $element->type = Element::TYPE_FILE;
    $element->class = 'jquery-file-upload';
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  public function BuildInputElement($name, $title, $id, $icon)
  {
    $element = new InputElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = '';
    $element->title = $title;
    $element->type = Element::TYPE_INPUT;
    $element->class = 'text-box';
    $element->icon = $icon;
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  
  public function BuildEmailElement($name, $title, $id)
  {
    $element = new InputElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = '';
    $element->title = $title;
    $element->type = Element::TYPE_EMAIL;
    $element->class = 'text-box';
    $element->icon = 'icon-communication-062';
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  
  public function BuildPhoneNumberElement($name, $title, $id)
  {
    $element = new InputElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = '';
    $element->title = $title;
    $element->type = Element::TYPE_TEL;
    $element->class = 'text-box';
    $element->icon = 'icon-communication-006';
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  
  public function BuildPasswordElement($name, $title, $id)
  {
    $element = new InputElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = '';
    $element->title = $title;
    $element->type = Element::TYPE_PASSWORD;
    $element->class = 'text-box password';
    $element->icon = 'icon-media-094';
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  
  public function BuildCheckboxElement($name, $title, $id)
  {
    $element = new CheckBoxElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = '';
    $element->title = $title;
    $element->type = Element::TYPE_CHECKBOX;
    $element->class = 'checkbox';
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  
  public function BuildCustomHmlElement($name, $title, $id)
  {
    $element = new CustomHtmlElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = '';
    $element->title = $title;
    $element->type = '';
    $element->class = 'form-custom-html-element';
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  public function BuildSubmitButton($name, $title, $id)
  {
    $element = new SubmitElement();
    $element->id = $id;
    $element->name = $name;
    $element->value = $title;
    $element->title = $title;
    $element->type = Element::TYPE_SUBMIT;
    $element->class = 'submit-form-btn';
    
    $this->elements[$name] = $element;
    
    return $this;
  }
  
  public function Render()
  {
    $html = '';
    
    $html .= '<div id="form-container-' . $this->formId . '" class="form-container" style="display: none;">';
        $html .= '<div class="form-container-title">'
                 . '<span class="title">' . $this->title . '</span>'
                 . '<div><a href="#" class="fa fa-close form-close" onclick="window.' . $this->formId . '.close(); return false;"></a></div>'
               . '</div>';
    $html .= '<form id="' . $this->formId .'" class="modal-form ' . $this->formClass . '" enctype="multipart/form-data">';
    $html .= '<div class="error-container" style="display: none;"></div>';
    $html .= '<div class="loading-overlay"></div>';
    foreach ($this->elements as $element)
    {
      $html .= $element->Render();
    }
    
    $html .= '</form>';
    $html .= '</div>';
    return $html;
  }
};


class ClassicForm extends FormModal
{
  public function Render()
  {
    $html = '';
    $html .= '<div id="form-container-' . $this->formId . '" class="form-container">';
    $html .= '<form id="' . $this->formId .'" class="modal-form ' . $this->formClass . '" enctype="multipart/form-data">';
    $html .= '<h2 class="h3 g-color-black mb-4">' . $this->title . '</h2>';
    $html .= '<div class="error-container" style="display: none;"></div>';
    $html .= '<div class="loading-overlay"></div>';
    foreach ($this->elements as $element)
    {
      $html .= $element->Render();
    }
    
    $html .= '</form>';
    $html .= '</div>';
    return $html;
  }
  
  public function PrintForm()
  {
    print $this->Render();
    $script = '<script>$(document).ready(function(){window.' . $this->formId . ' = new ClassicForm(\'' . $this->formId . '\'); window.' . $this->formId . '.init();});'
            . '$( "#' . $this->formId . '" ).submit(function( event ) {
    event.preventDefault();
    var pars = window.' . $this->formId . '.collectFormParams();
    window.' . $this->formId . '.showLoadingProgress();
    if (window.' . $this->formId . '.fileUpload.haveFileUpload)
    {
      window.' . $this->formId . '.fileUpload.ajaxData.url = "' . $this->url . '";
      window.' . $this->formId . '.fileUpload.ajaxData.pars = pars;
      window.' . $this->formId . '.fileUpload.ajaxData.fnSuccess = function(response){if(response.redirect) window.location.href = response.redirect; else location.reload();};
      window.' . $this->formId . '.fileUpload.ajaxData.fnError = function(response){ if(response.error) window.' . $this->formId . '.setError(response.error); window.' . $this->formId . '.hideLoadingProgress();};
      window.' . $this->formId . '.fileUpload.data.submit();
    }
    else
      AjaxRequest("' . $this->url . '", pars, function(response){if(response.redirectUrl) window.location = response.redirectUrl;}, function(response){ if(response.error) window.' . $this->formId . '.setError(response.error); window.' . $this->formId . '.hideLoadingProgress();});
  });'
            . '</script>';
    print $script;
  }
  
  public function SetApiUrl($apiUrl)
  {
    $this->url = $apiUrl;
  }
}

?>

