<?php

global $URL_TO_CONTROLLER;
$URL_TO_CONTROLLER = array(
  ''      => 'index',
  'index' => 'index',
  'login' => 'login',
  'signup' => 'signup',
  'upload' => 'upload',
  'video/[a-z0-9]+/?' => 'video',
  'category/[a-z0-9-]+/?' => 'category',
  'channel/[a-z0-9]+/?' => 'channel',
  'example' => 'example',
  'laptop/[a-z0-9]+/?' => 'laptop',
    
  /*------ ADMIN--------*/  
  'panel'      => 'admin-conversions',
  'panel/users' => 'admin-users',
  'panel/conversions' => 'admin-conversions',
);

$CONTROLLER_TO_URL = array(
  'index' => '',
  'login' => 'login',
  'signup' => 'signup',
  'upload' => 'upload',
  'video' => 'video',
  'category' => 'category',
  'channel' => 'channel',
  'example' => 'example',
  'laptop' => 'laptop',
    
  /*------ ADMIN ------ */
  'panel'       => 'panel/conversions',
  'admin-users' => 'panel/users',
  'admin-conversions' => 'panel/conversions',
);

global $CONTROLLERS;
$CONTROLLERS = array(
  'index'        => CONTROLLERS_PATH . 'index.php', //
  'login'        => CONTROLLERS_PATH . 'login.php', //
  'signup'        => CONTROLLERS_PATH . 'signup.php', //
  'upload'        => CONTROLLERS_PATH . 'upload.php', //
  'video'        => CONTROLLERS_PATH . 'video.php', //
  'category'        => CONTROLLERS_PATH . 'category/categories.php', //
  'channel'        => CONTROLLERS_PATH . 'channel.php', //
  'example' => './system/forms/examples/form-elements.php',
  'laptop'        => CONTROLLERS_PATH . 'laptop/view.php', //
    
  /*------ ADMIN ------*/
  'admin-users' => CONTROLLERS_PATH . 'admin/users.php', //
  'admin-conversions' => CONTROLLERS_PATH . 'admin/conversions.php', //
);

