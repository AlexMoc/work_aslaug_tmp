<?php
  define('LOGS_PATH', './system/logs/');
  define('GENERIC_LOG_FILE', LOGS_PATH . 'log.txt');
  define('DB_ERRORS_LOG_FILE', LOGS_PATH . 'errors.txt');
  define('CONVERSION_LOG_FILE', LOGS_PATH . 'conversions.txt');
  
  define('ROOT_FOLDER', '/');
  define('URL_FINGERPRINT', '?f=' . date('iHdms'));
  define('COOKIE_NAME', 'ASLAUG_TO_SOLVE_A_PUZZLE');//
  define('ANONIM_USER', 'ASLAUG_DEFAULT_ANONIM_USER');
  
  define('CONTROLLERS_PATH', './system/controllers/');
  define('VIEWS_PATH', './system/views/');
  define('TOOLS_PATH', './system/tools/');
  
  define('TMP_UPLOADED_IMG_PATH', 'images/uploads/tmp/');
  define('TMP_UPLOADED_FILES_PATH', 'files/uploads/tmp/');
  define('CONVERTED_FILES_PATH', 'files/converted/');
  define('CHANNEL_FILES_PATH', 'files/videos/channel/');
  define('FILES_UPLOADED_IMAGES_PATH', TMP_UPLOADED_IMG_PATH . 'uploaded-images/');
  
  define('REQUST_SUCCESS', 200);
  define('REQUST_ERROR', 400);
  
  define("VIDEO_ACTIVITIES_FEED_SIZE", 10);
  
  define("FFMPEG_COMMAND", 'ffmpeg');
?>

