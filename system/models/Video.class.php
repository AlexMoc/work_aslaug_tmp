<?php

class Video {
  
  /** @var Integer */
  public $id;
  /** @var String */
  public $title;
  /** @var String */
  public $description;
  /** @var Integer */
  public $userId;
  /** @var String */
  public $videoUrl;
  /** @var String */
  public $category;
  /** @var String */
  public $fileSize;
  /** @var String */
  public $dateUploaded;
  /** @var String */
  public $status;
  /** @var String */
  public $thumbUrl;
  /** @var String */
  public $videoThumbUrl;
  /** @var String */
  public $views;
  
  const STATUS_PUBLIC = 'public';
  const STATUS_PRIVATE = 'private';
  
  static $STATUS_TITLES = array(
    self::STATUS_PUBLIC => 'Public',
    self::STATUS_PRIVATE => 'Private',
  );
  
  const CATEGORY_HISTORY = 'history';
  const CATEGORY_ART = 'art';
  const CATEGORY_WISDOM = 'wisdom';
  const CATEGORY_EDUCATION = 'education';
  const CATEGORY_SCIENCE = 'science';
  const CATEGORY_NEWS = 'news';
  const CATEGORY_GAMES = 'games';
  const CATEGORY_ENTERTAINMENT = 'entertainment';
  const CATEGORY_COMPUTER_SCIENCE = 'computer-science';
  
  static $CATEGORY_TITLES = array(
    self::CATEGORY_HISTORY => 'History',
    self::CATEGORY_ART => 'Art',
    self::CATEGORY_WISDOM => 'Wisdom',
    self::CATEGORY_EDUCATION => 'Education',
    self::CATEGORY_SCIENCE => 'Science',
    self::CATEGORY_NEWS => 'News',
    self::CATEGORY_GAMES => 'Games',
    self::CATEGORY_ENTERTAINMENT => 'Entertainment',
    self::CATEGORY_COMPUTER_SCIENCE => 'Computer Science',
  );
  
  public function __construct($id = null)
  {
    $this->Init();
    if (!ValidId($id) || !$this->Load($id))
      $this->Init();
  } 
  
  private function Init()
  {
    $this->id = -1;
  }
  
  private function Load($id = null)
  {
    if (!$this->LoadData($id))
      return false;
    return true;
  }
  
  protected function LoadData($id = null)
  {
    if (!ValidId($id))
      return false;

    $query = "SELECT ALL v.* "
             . " FROM videos AS v"
             . " WHERE v.id=" . ToSqlQuotedString($id);
    $rows = ExecuteQuery($query);
    // return false if nothing loaded
    if (count($rows) != 1)
      return false;
    $this->SetPropertyValues($rows[0]);
    return true;
  }
  
  /**
   * convert database values to object values
   * @param Array $values
   */
  protected function SetPropertyValues($values)
  {
    $this->id = $values['id'];
    $this->userId = $values['user_id'];
    $this->title = $values['title'];
    $this->description = $values['description'];
    $this->videoUrl = $values['video_url'];
    $this->category = $values['category'];
    $this->fileSize = $values['file_size'];
    $this->dateUploaded = $values['date_uploaded'];
    $this->status = $values['status'];
    $this->thumbUrl = $values['thumb_url'];
    $this->videoThumbUrl = $values['video_thumb_url'];
    $this->views = $values['views'];
  }
  
  
  static public function LoadVideos($where, $joins = null, $suffix =null, &$paginationData = array())
  {
    $query = 'SELECT * FROM videos AS v '
            . $joins
            . ' WHERE ' . $where 
            . ' ' . $suffix;
    
    if (!empty($paginationData))
      $rows = ExecutePaginatedQuery($query, $paginationData);
    else
      $rows = ExecuteQuery($query);
    
    $videos = array();
    foreach ($rows as $row)
    {
      $newVideo = new Video();
      $newVideo->SetPropertyValues($row);
      $videos[] = $newVideo;
    }
    
    return $videos;
  }
  
  /**
   * 
   * @return VideoStats
   */
  public function GetStats()
  {
    if (!ValidId($this->id))
      return null;
    
    $videoStats = new VideoStats($this->id);
    if (!ValidId($videoStats->videoId))
    {
      $videoStats->videoId = $this->id;
      $videoStats->dateUpdated = FormatSqlDatetime(time());
      $videoStats->Save();
    }
    return $videoStats;
  }
  
  static public function LoadTrendingVideos($user = null)
  {
    $where = 'v.status!=' . ToSqlQuotedString(Video::STATUS_PRIVATE);
    return self::LoadVideos($where, null, "ORDER BY v.id DESC LIMIT 4");
  }
  
  static public function LoadRecomendedVideos($video = null, $user = null)
  {
    $where = 'v.status!=' . ToSqlQuotedString(Video::STATUS_PRIVATE)
           . ' AND v.id!=' . ToSqlQuotedString($video->id);
    $suffix = 'ORDER BY v.category=' . ToSqlQuotedString($video->category)
            . ', v.views DESC, v.id DESC';
    return self::LoadVideos($where, null, $suffix);
  }
  
  public function Save()
  {
    $values = array();
    $values['user_id']      = $this->userId;
    $values['title']      = $this->title;
    $values['description']      = $this->description;
    $values['video_url']      = $this->videoUrl;
    $values['category']      = $this->category;
    $values['file_size']      = $this->fileSize;
    $values['date_uploaded']      = $this->dateUploaded;
    $values['status']      = $this->status;
    $values['thumb_url']      = $this->thumbUrl;
    $values['video_thumb_url']      = $this->videoThumbUrl;
    $values['views']      = $this->views;
   
    $result = false;
    if (ValidId($this->id))
    {
      $result = UpdateRow('videos', $values, 'id=' . ToSqlQuotedString($this->id));
    }
    else
    {
      //$values['password'] = self::HashPassword($this->password);
      $result = InsertRow('videos', $values);
      if ($result)
        $this->id = GetLastInsertId('videos');
    }
    return $result;
  }
  
  public function GetThumbUrl()
  {
    if ($this->thumbUrl)
      return GenerateImageAbsolutePath ($this->thumbUrl);
    else if ($this->videoThumbUrl)
      return GenerateImageAbsolutePath ($this->videoThumbUrl);
    
    return "";
  }
}


class VideoStats
{
  /** @var Integer */
  public $id;
  /** @var Integer */
  public $videoId;
  /** @var Integer */
  public $views;
  /** @var Integer */
  public $likesCount;
  /** @var Integer */
  public $dislikesCount;
  /** @var String */
  public $dateUpdated;
  
  public function __construct($id = null)
  {
    $this->Init();
    if (!ValidId($id) || !$this->Load($id))
      $this->Init();
  } 
  
  private function Init()
  {
    $this->id = -1;
    $this->likesCount = 0;
    $this->dislikesCount = 0;
  }
  
  private function Load($id = null)
  {
    if (!$this->LoadData($id))
      return false;
    return true;
  }
  
  protected function LoadData($id = null)
  {
    if (!ValidId($id))
      return false;

    $query = "SELECT ALL vs.* "
             . " FROM video_stats AS vs"
             . " WHERE vs.video_id=" . ToSqlQuotedString($id);
    $rows = ExecuteQuery($query);
    // return false if nothing loaded
    if (count($rows) != 1)
      return false;
    $this->SetPropertyValues($rows[0]);
    return true;
  }
  
  /**
   * convert database values to object values
   * @param Array $values
   */
  protected function SetPropertyValues($values)
  {
    $this->id = $values['id'];
    $this->videoId = $values['video_id'];
    $this->likesCount = $values['likes'];
    $this->dislikesCount = $values['dislikes'];
    $this->views = $values['views'];
    $this->dateUpdated = $values['date_updated'];
  }
  
  public function Save()
  {
    $values = array();
    $values['video_id']      = $this->videoId;
    $values['views']      = $this->views;
    $values['likes']      = $this->likesCount;
    $values['dislikes']      = $this->dislikesCount;
    $values['date_updated']      = $this->dateUpdated;
   
    $result = false;
    if (ValidId($this->id))
    {
      $result = UpdateRow('video_stats', $values, 'id=' . ToSqlQuotedString($this->id));
    }
    else
    {
      //$values['password'] = self::HashPassword($this->password);
      $result = InsertRow('video_stats', $values);
      if ($result)
        $this->id = GetLastInsertId('video_stats');
    }
    return $result;
  }
}

