<?php

class Like {
  
  /** @var Integer */
  public $id;
  /** @var Integer */
  public $userId;
  /** @var Integer */
  public $likedEntityId;
  /** @var Integer */
  public $likedType;
  /** @var Integer */
  public $likeCategory;

  
  const ENTITY_VIDEO = 1;
  const ENTITY_COMMENT = 2;
  
  static $ENTITY_TITLES = array(
    self::ENTITY_VIDEO => 'Video',
    self::ENTITY_COMMENT => 'Comment',
  );
  
  const CATEGORY_LIKE = 1;
  const CATEGORY_DISLIKE = 2;
  
  static $CATEGORY_TITLES = array(
    self::CATEGORY_LIKE => 'Like',
    self::CATEGORY_DISLIKE => 'Dislike',
  );
  
  public function __construct($id = null)
  {
    $this->Init();
    if (!ValidId($id) || !$this->Load($id))
      $this->Init();
  } 
  
  private function Init()
  {
    $this->id = -1;
  }
  
  private function Load($id = null)
  {
    if (!$this->LoadData($id))
      return false;
    return true;
  }
  
  protected function LoadData($id = null)
  {
    if (!ValidId($id))
      return false;

    $query = "SELECT ALL l.* "
             . " FROM likes AS l"
             . " WHERE l.id=" . ToSqlQuotedString($id);
    $rows = ExecuteQuery($query);
    // return false if nothing loaded
    if (count($rows) != 1)
      return false;
    $this->SetPropertyValues($rows[0]);
    return true;
  }
  
  static public function LoadLikes($where, $joins = null, $suffix =null, &$paginationData = array())
  {
    $query = 'SELECT * FROM likes AS l '
            . $joins
            . ' WHERE ' . $where 
            . ' ' . $suffix;
    
    if (!empty($paginationData))
      $rows = ExecutePaginatedQuery($query, $paginationData);
    else
      $rows = ExecuteQuery($query);
    
    $likes = array();
    foreach ($rows as $row)
    {
      $newLike = new Like();
      $newLike->SetPropertyValues($row);
      $likes[] = $newLike;
    }
    
    return $likes;
  }
  
  /**
   * convert database values to object values
   * @param Array $values
   */
  protected function SetPropertyValues($values)
  {
    $this->id            = $values['id'];
    $this->userId        = $values['user_id'];
    $this->likedEntityId = $values['liked_entity_id'];
    $this->likedType     = $values['like_type'];
    $this->likeCategory  = $values['like_category'];
    $this->date          = $values['date'];
  }
  
  public function Save()
  {
    $values = array();
    $values['user_id']         = $this->userId;
    $values['liked_entity_id'] = $this->likedEntityId;
    $values['like_type']      = $this->likedType;
    $values['like_category']   = $this->likeCategory;
    $values['date']            = $this->date;
   
    $result = false;
    if (ValidId($this->id))
    {
      $result = UpdateRow('likes', $values, 'id=' . ToSqlQuotedString($this->id));
    }
    else
    {
      //$values['password'] = self::HashPassword($this->password);
      $result = InsertRow('likes', $values);
      if ($result)
        $this->id = GetLastInsertId('likes');
    }
    return $result;
  }
  
  /**
   * 
   * @param User $user
   * @param mixed $entity
   * @return Like
   */
  public static function LoadPrevEntityLike($user, $entity)
  {
    $where = 'user_id=' . ToSqlQuotedString($user->id)
         . ' AND liked_entity_id=' . ToSqlQuotedString($entity->id);
    $suffix = ' LIMIT 1';
    $likes = self::LoadLikes($where, null, $suffix);
    
    return (isset($likes[0]) && ValidId($likes[0]->id)) ? $likes[0] : new Like();
  }
  
  public function Delete()
  {
    if (!ValidId($this->id))
      return false;
    
    return DeleteRow('likes', 'id=' . ToSqlQuotedString($this->id));
  }
}

