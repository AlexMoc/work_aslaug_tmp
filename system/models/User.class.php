<?php

class User {
  
  /** @var Integer */
  public $id;
  /** @var String */
  public $name;
  /** @var String */
  public $userName;
  /** @var String */
  public $email;
  /** @var String */
  public $phoneNumber;
  /** @var String */
  public $password;
  /** @var Boolean */
  public $type;
  /** @var String */
  public $termsAccepted;
  /** @var String */
  public $newsletterSubscribe;
  /** @var String */
  public $imageUrl;
  /** @var String */
  public $facebookUserId;
  /** @var String */
  public $facebookAccessToken;
  /** @var String */
  public $date;
  /** @var String */
  public $dateUpdated;
  
  const TYPE_USER = 'user';
  const TYPE_ADMIN = 'admin';
  
  public function __construct($id = null)
  {
    $this->Init();
    if (!ValidId($id) || !$this->Load($id))
      $this->Init();
  } 
  
  private function Init()
  {
    $this->id = -1;
  }
  
  private function Load($id = null)
  {
    if (!$this->LoadData($id))
      return false;
    return true;
  }
  
  protected function LoadData($id = null)
  {
    if (!ValidId($id))
      return false;

    $query = "SELECT ALL u.* "
             . " FROM users AS u"
             . " WHERE id=" . ToSqlQuotedString($id);
    $rows = ExecuteQuery($query);
    // return false if nothing loaded
    if (count($rows) != 1)
      return false;
    $this->SetPropertyValues($rows[0]);
    return true;
  }
  
  public static function LoadUser($id)
  {
    return new User($id);
  }
  
  /**
   * convert database values to object values
   * @param Array $values
   */
  protected function SetPropertyValues($values)
  {
    $this->id = $values['id'];
    $this->name = $values['name'];
    $this->userName = $values['user_name'];
    $this->email = $values['email'];
    $this->phoneNumber = $values['phone_number'];
    $this->password = $values['password'];
    $this->type = $values['type'];
    $this->imageUrl = $values['image_url'];
    $this->facebookAccessToken = $values['facebook_access_token'];
    $this->facebookUserId = $values['facebook_user_id'];
    $this->termsAccepted = $values['terms_accepted'];
    $this->newsletterSubscribe = $values['newsletter_subscribe'];
  }
  
  static public function LoadUserByEmail($email)
  {
    if (strlen($email) > 0)
    {
      $query = "SELECT ALL u.* "
             . " FROM users AS u"
             . " WHERE u.email=" . ToSqlQuotedString($email);
      $rows = ExecuteQuery($query);
    }
    $user = new User();
    if (count($rows) > 0)
      $user->SetPropertyValues($rows[0]);
    
    return $user;
  }
  
  static public function LoadUserByFacebookId($fbUserId)
  {
    if (strlen($fbUserId) > 0)
    {
      $query = "SELECT ALL u.* "
             . " FROM users AS u"
             . " WHERE u.facebook_user_id=" . ToSqlQuotedString($fbUserId);
      $rows = ExecuteQuery($query);
    }
    $user = new User();
    if (count($rows) > 0)
      $user->SetPropertyValues($rows[0]);
    
    return $user;
  }
  
  static public function LoadUsers($where, $joins = null, $suffix =null, &$paginationData = array())
  {
    $query = 'SELECT * FROM users AS u '
            . $joins
            . ' WHERE ' . $where 
            . ' ' . $suffix;
    
    if (!empty($paginationData))
      $rows = ExecutePaginatedQuery($query, $paginationData);
    else
      $rows = ExecuteQuery($query);
    
    $users = array();
    foreach ($rows as $row)
    {
      $newUser = new User();
      $newUser->SetPropertyValues($row);
      $users[] = $newUser;
    }
    
    return $users;
  }
  
  public function Save()
  {
    $values = array();
    $values['name']      = $this->name;
    $values['user_name']      = $this->userName;
    $values['email']     = $this->email;
    $values['phone_number']     = $this->phoneNumber;
    $values['password']  = $this->password;
    $values['type']      = $this->type;
    $values['image_url']      = $this->imageUrl;
    $values['facebook_access_token']      = $this->facebookAccessToken;
    $values['facebook_user_id']      = $this->facebookUserId;
    $values['terms_accepted']      = $this->termsAccepted;
    $values['newsletter_subscribe']      = $this->newsletterSubscribe;
    $values['date']      = $this->date;
    $values['date_updated']      = $this->dateUpdated;
   
    $result = false;
    if (ValidId($this->id))
    {
      $result = UpdateRow('users', $values, 'id=' . ToSqlQuotedString($this->id));
    }
    else
    {
      //$values['password'] = self::HashPassword($this->password);
      $result = InsertRow('users', $values);
      if ($result)
        $this->id = GetLastInsertId('users');
    }
    return $result;
  }
  
  public function IsAdmin()
  {
    return $this->type == User::TYPE_ADMIN;
  }
  
  public function SetFacebookData($data)
  {
    $this->facebookAccessToken = $data['accessToken'];
    $this->facebookUserId = $data['fbId'];
  }
}

