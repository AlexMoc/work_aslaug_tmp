<?php

class VideoActivity {
  
  /** @var Integer */
  public $id;
  /** @var Integer */
  public $parentId;
  /** @var Integer */
  public $userId;
  /** @var Integer */
  public $videoId;
  /** @var Integer */
  public $likesCount;
  /** @var Integer */
  public $dislikesCount;
  /** @var String */
  public $comment;
  /** @var String */
  public $date;
  /** @var String */
  public $status;

  
  const STATUS_NEW = 'new';
  const STATUS_DELETED = 'deleted';
  
  static $STATUS_TITLES = array(
    self::STATUS_NEW => 'New',
    self::STATUS_DELETED => 'Deleted',
  );
  
  public function __construct($id = null)
  {
    $this->Init();
    if (!ValidId($id) || !$this->Load($id))
      $this->Init();
  } 
  
  private function Init()
  {
    $this->id = -1;
  }
  
  private function Load($id = null)
  {
    if (!$this->LoadData($id))
      return false;
    return true;
  }
  
  protected function LoadData($id = null)
  {
    if (!ValidId($id))
      return false;

    $query = "SELECT ALL va.* "
             . " FROM video_activities AS va"
             . " WHERE va.id=" . ToSqlQuotedString($id);
    $rows = ExecuteQuery($query);
    // return false if nothing loaded
    if (count($rows) != 1)
      return false;
    $this->SetPropertyValues($rows[0]);
    return true;
  }
  
  static public function LoadVideoActivities($where, $joins = null, $suffix =null, &$paginationData = array())
  {
    $query = 'SELECT * FROM video_activities AS va '
            . $joins
            . ' WHERE ' . $where 
            . ' ' . $suffix;
    
    if (!empty($paginationData))
      $rows = ExecutePaginatedQuery($query, $paginationData);
    else
      $rows = ExecuteQuery($query);
    
    $videoActivities = array();
    foreach ($rows as $row)
    {
      $newVideoActivity = new VideoActivity();
      $newVideoActivity->SetPropertyValues($row);
      $videoActivities[] = $newVideoActivity;
    }
    
    return $videoActivities;
  }
  
  /**
   * convert database values to object values
   * @param Array $values
   */
  protected function SetPropertyValues($values)
  {
    $this->id      = $values['id'];
    $this->parentId= $values['parent_id'];
    $this->userId  = $values['user_id'];
    $this->videoId = $values['video_id'];
    $this->comment = $values['comment'];
    $this->status  = $values['status'];
    $this->date    = $values['date'];
    $this->likesCount    = $values['likes_count'];
    $this->dislikesCount    = $values['dislikes_count'];
  }
  
  public function Save()
  {
    $values = array();
    $values['parent_id']= $this->parentId;
    $values['user_id']  = $this->userId;
    $values['video_id'] = $this->videoId;
    $values['comment']  = $this->comment;
    $values['status']   = $this->status;
    $values['date']     = $this->date;
    $values['likes_count']     = $this->likesCount;
    $values['dislikes_count']     = $this->dislikesCount;
   
    $result = false;
    if (ValidId($this->id))
    {
      $result = UpdateRow('video_activities', $values, 'id=' . ToSqlQuotedString($this->id));
    }
    else
    {
      //$values['password'] = self::HashPassword($this->password);
      $result = InsertRow('video_activities', $values);
      if ($result)
        $this->id = GetLastInsertId('video_activities');
    }
    return $result;
  }
  
  public function Delete()
  {
    if (!ValidId($this->id))
      return false;
    
    $values = array(
      'status' => VideoActivity::STATUS_DELETED
    );
    
    return UpdateRow('video_activities', $values, 'id=' . ToSqlQuotedString($this->id));
  }
}

