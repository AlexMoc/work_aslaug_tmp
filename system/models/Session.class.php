<?php

class Session
{
  public $sessionId;
  public $userId;
  public $timestampCreated;
  public $lastActivity;
  public $isUserLoggedIn;
  
  /** User type */
  private $user;
  /**
   * 
   * @param User $user
   */
  function __construct($user = null, $sessionId = null) 
  {
    if ($sessionId)
    {
      $this->LoadData($sessionId);
      $this->user = new User($this->userId);
      return $this;
    }
    else
    {
      if (ValidId($user->id))
        $this->user = $user;
      $this->Init();
      $this->CreateSession();
    }
  }
  
  private function Init()
  {
    $this->sessionId = '';
    $this->userId = -1;
    $this->timestampCreated = 0;
    $this->isUserLoggedIn = false;
  }
  
  private function CreateSession()
  {
    $this->sessionId = md5('USER_SESSION' . $this->user->id);
    $this->userId = $this->user->id;
    $this->timestampCreated = time();
    $this->isUserLoggedIn = true;
    
    $this->Save();
  }
  
  protected function LoadData($id = null)
  {
    if (empty($id))
      return false;

    $query = "SELECT s.* "
             . " FROM sessions AS s"
             . " WHERE session_id=" . ToSqlQuotedString($id);
    $rows = ExecuteQuery($query);
    // return false if nothing loaded
    if (count($rows) != 1)
      return false;
    $this->SetPropertyValues($rows[0]);
    return true;
  }
  
  private function SetPropertyValues($values)
  {
    $this->sessionId = $values['session_id'];
    $this->userId    = $values['user_id'];
    $this->timestampCreated = $values['time_stamp'];
    $this->lastActivity = $values['last_activity'];
  }
  
  private static function GetSessionId()
  {
    if (defined('SESSION_COOKIE_NAME') && isset($_COOKIE[SESSION_COOKIE_NAME]))
      return $_COOKIE[SESSION_COOKIE_NAME];
    return null;
  }
  
  private function ValidateSessionData()
  {
    if (empty($this->sessionId))
      return false;
    
    return true;
  }
  
  private function Delete()
  {
    if (empty($this->sessionId)) return false;

    $where = 'session_id=' . ToSqlQuotedString($this->sessionId);
    return DeleteRow('sessions', $where);
  }
  
  public function InitAnonymousUserSession($sessionId)
  {
    // get anonymous use session id from cookies
    $cookieName = 'anonymousUserSessionId';
    $anonymousSessionId = $_COOKIE[$cookieName];

    if ($this->isUserLoggedIn)
    {
      if ($anonymousSessionId)
      {
        $this->ResetUserSessionData($anonymousSessionId); // delete anonymous user data
        DeleteCookieValue($cookieName, true); // remove anonoymous cookie
        $this->anonymousUserSessionId = null;
      }
    }
    else
    {
      // if sessionId given, the user has been logged out just now.
      if ($sessionId)
        $this->ResetUserSessionData($anonymousSessionId); // delete authenticated user data

      // otherwise if no anonymous session found, create one.
      if (!$anonymousSessionId)
      {
        $anonymousSessionId = base64_encode(time()); // shorter session id string in base 64
//        return SetCookieValue($cookieName, $anonymousSessionId, self::GetCookieExpiresDate(), true);
        return setcookie($cookieName, $anonymousSessionId, null, '/');
      }
      $this->anonymousUserSessionId = $anonymousSessionId;
    }
  }
  
  public function Save()
  {
    if (empty($this->sessionId) || empty($this->userId)) return false;
    $values = array();
    $values['session_id']   = $this->sessionId;
    $values['user_id']      = $this->userId;
    $values['time_stamp']   = date('Y-m-d H:i:s', $this->timestampCreated);
    global $CURRENT_USER;
    $CURRENT_USER = $this->user;
    return ReplaceRow('sessions', $values);
  }
  
  public function GetSessionUser()
  {
    return $this->user;
  }
  
  public function LogoutUser()
  {
    setcookie(COOKIE_NAME, NULL, NULL, '/');
  }
          
}

