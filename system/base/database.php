<?php

// Initiate connection
global $dbCon;
$dbCon = @DbConnect($dbServer, $dbName, $dbUser, $dbPassword);

// Connect to DB and return the connection.
// Returns NULL on failure.
function DbConnect($server, $dbName, $user, $password)
{
  $con = mysqli_connect($server, $user, $password, $dbName, 3306);
//  $db = @mysql_select_db($name, $con);

  if(!$con)
  {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
    return NULL;
  }
  else
  {
    mysqli_query($con, "SET NAMES 'utf8'");
    return $con;
  }
}

// Disconnect form DB.
function DbDisconnect($dbCon)
{
  $res = mysqli_close($dbCon);
  if(!$res)
    return false;
  else
    return true;
}


function &ExecuteQuery($query)
{
  global $dbCon;
  $result = array();
  $rows = mysqli_query($dbCon, $query);
  
  if ($rows === FALSE)
  {
    LogDbErrorToFile($query);
    return $result;
  }
  
  $numFields = mysqli_num_fields($rows);
  $rowNum = 0;
  while ($row = mysqli_fetch_array($rows))
  {
    for ($i = 0; $i < $numFields; $i++)
    {
      $fieldName = mysqli_field_name($rows, $i);
      $result[$rowNum][$fieldName] = $row[$i];
    }
    $rowNum++;
  }
  //DebugQuery($query, $timer, $rowNum);
  return $result;
}

function mysqli_field_name($result, $field_offset)
{
    $properties = mysqli_fetch_field_direct($result, $field_offset);
    return is_object($properties) ? $properties->name : null;
}

function &ExecutePaginatedQuery(&$query, &$paginationData = array())
{
  $from = intval($paginationData['from']);
  $length = intval($paginationData['length']);
  $limit = '';
  if (intval($length) > 0)
    $limit = " LIMIT $from, $length";
  else if ($from > 0)
    $limit = " LIMIT $from, 18446744073709551615"; // set to a big number

  $query = trim($query);
  // if query starts with SELECT statement we can insert SQL_CALC_FOUND_ROWS prefix.s
  if (stripos($query, "SELECT ") === 0)
  {
    $query = substr($query, 7);
    // as stated in mysql 5 manual, the best way to find total number of rows is
    // to add SQL_CALC_FOUND_ROWS prefix
    $result = ExecuteQuery("SELECT SQL_CALC_FOUND_ROWS $query $limit");

    // get the total number of rows in a separate query.
    $rows = ExecuteQuery("SELECT FOUND_ROWS() AS total");
    $paginationData['total'] = $rows[0]['total'];
  }
  else
    // otherwise dont count total number of rows
    $result = ExecuteQuery("$query $limit");

  return $result;
}

function ExecuteNonQuery($query)
{
  global $dbCon;
  $res = mysqli_query($dbCon, $query);

  if ($res === FALSE)
    LogDbErrorToFile($query);
  
  if ($res)
    return true;
  else
    return false;
}

function GetLastInsertId($table)
{
  $res = ExecuteQuery("SELECT LAST_INSERT_ID() as id FROM `$table` LIMIT 0,1");
  return $res[0]['id'];
}

function InsertRow($table, $values)
{
  $strColumns = '';
  $strValues = '';
  foreach ($values as $column=>$value)
  {
    $column = trim($column);
    if(is_null($value))
      $value = 'NULL';
    else
      $value = ToSqlQuotedString($value);

    $strColumns .= ("$strColumns" != '' ? ',' : '') . "`$column`";
    $strValues .= ($strValues != '' ? ',' : '') . $value;
  }
  $query = "INSERT IGNORE INTO `$table` ($strColumns) values ($strValues)";
  
  return ExecuteNonQuery($query);
}

function UpdateRow($table, $values, $where = 1)
{
  $strValues = '';
  if(!isset($where)) $where = 1;
  foreach ($values as $column=>$value)
  {
    $column = trim($column);
    if(is_null($value))
      $value = 'NULL';
    else
      $value = ToSqlQuotedString($value);
      
    $strValues .= ($strValues != '' ? ',' : '') . "`$column` = $value";
  }
  $query = "UPDATE `$table` SET $strValues WHERE $where";
  return ExecuteNonQuery($query);
}

/**
 * @param type $table
 * @param type $values
 * @return type
 */
function ReplaceRow($table, $values)
{
  $strColumns = '';
  $strValues = '';
  foreach ($values as $column=>$value)
  {
    $column = trim($column);
    if (is_null($value))
      $value = 'NULL';
    else
      $value = ToSqlQuotedString($value);

    $strColumns .= ($strColumns != '' ? ',' : '') . $column;
    $strValues .= ($strValues != '' ? ',' : '') . $value;
  }
  $query = "REPLACE INTO `$table` ($strColumns) values ($strValues)";
  return ExecuteNonQuery($query);
}

function RowExists($table, $where=1)
{
  global $dbCon;
  if(!isset($where)) $where = 1;
  $query = "SELECT * FROM `$table` WHERE $where LIMIT 1";
  $rows = mysqli_query($dbCon, $query);
  if (mysqli_num_rows($rows) > 0)
    return true;
  else
  {
    return false;
  }
}

function DeleteRow($table, $where=1)
{
  if(!isset($where)) $where = 1;
  $query = "DELETE FROM `$table` WHERE $where";
  return ExecuteNonQuery($query);
}

function FormatSqlString($value)
{
  global $dbCon;
  return mysqli_real_escape_string($dbCon, $value);
}
function ToSqlQuotedString($value)
{
  return "'".FormatSqlString($value)."'";
}

function FormatSqlDate($timestamp)
{
  return date('Y-m-d', $timestamp);
}

function FormatSqlDatetime($timestamp)
{
  return date('Y-m-d H:i:s', $timestamp);
}

function time_elapsed_string($datetime, $full = false) 
{
  $now = new DateTime;
  $ago = new DateTime($datetime);
  $diff = $now->diff($ago);

  $diff->w = floor($diff->d / 7);
  $diff->d -= $diff->w * 7;

  $string = array(
      'y' => 'year',
      'm' => 'month',
      'w' => 'week',
      'd' => 'day',
      'h' => 'hour',
      'i' => 'minute',
      's' => 'second',
  );
  foreach ($string as $k => &$v) {
      if ($diff->$k) {
          $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
      } else {
          unset($string[$k]);
      }
  }

  if (!$full) $string = array_slice($string, 0, 1);
  return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function LogDbErrorToFile($query)
{
  global $dbCon;
  $backtrace = debug_backtrace();
  $msg = "MySQL ".mysqli_errno($dbCon). ': '.mysqli_error($dbCon);
  if ($query !== null)
    $msg .= "\nquery: $query";
  
  foreach ($backtrace as $key => $value) 
  {
    $msg .= "\n[{$key}]File -> ".$value['file'].' (line: '.$value['line'].') function -> '.$value['function'];
  }
  LogInfoToFile($msg, DB_ERRORS_LOG_FILE);
}

function LogConversionInfoToFile($msg)
{
  LogInfoToFile($msg, CONVERSION_LOG_FILE);
}

function BuildSqlInClause($column, $values)
{
  if (is_array($values) && count($values) > 0)
  {
    $where = '';
    foreach ($values as $value)
      if ((string) trim($value) !== '')
        $where .= ($where == '' ? '' : ', ') . ToSqlQuotedString($value);
    if ($where)
      return "$column IN ($where)";
  }
  return " FALSE";
}

function BuildBasicAdminWhereClasue($paginationData, $tableAlias)
{
  return ' 1 ';
}
function BuildBasicAdminSuffixClasue($paginationData, $tableAlias)
{
  if (isset($paginationData['sortColumn']) && isset($paginationData['sortDir']))
  {
    return " ORDER BY $tableAlias." . $paginationData['sortColumn'] . " " . $paginationData['sortDir'];
  }
  
  return '';
}
