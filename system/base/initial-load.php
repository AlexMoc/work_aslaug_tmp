<?php
  require_once('./system/data/settings.php');
  require_once('./system/data/constants.php');
  require_once('./system/base/utils.php');
  require_once('./system/base/renders.php');
  require_once('./system/base/url-manager.php');
  
  require_once('./system/base/addings.php');
  require_once('./system/base/database.php');
  require_once('./system/data/url-dictionary.php');
  require_once('./system/models/Session.class.php');
  require_once('./system/models/User.class.php');
  require_once('./system/models/Video.class.php');
  require_once('./system/models/Like.class.php');
  require_once('./system/models/VideoActivity.class.php');
  require_once('./system/forms/modal-forms.php');
  require_once('./system/forms/user-forms.php');
  require_once('./system/forms/admin-forms.php');
  require_once('./system/views/includes/printers.inc.php');
  date_default_timezone_set('Europe/Chisinau');
  
  InitUserSesssion();
  
  require_once (VIEWS_PATH . 'print-page/print-page.php');
?>