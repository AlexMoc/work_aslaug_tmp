<?php
function LogInfoToFile($obj, $fileName = null)
{
  if (!$fileName)
    $fileName = GENERIC_LOG_FILE;
  if (file_exists($fileName))
    $file = fopen($fileName, 'a+');
  else
    $file = fopen($fileName, 'x+');

  if (is_array($obj) || is_object($obj))
    $obj = var_export($obj, true);
  $str = "[" . date('d-M-Y H:i:s') . '] ' . $obj;
  fwrite($file, $str."\n");
  fclose($file);
}
function GetRootFolder()
{
  return ROOT_FOLDER;
}

function start_content() 
{
  global $GET_CONTENT;
  $GET_CONTENT .= ob_get_contents();
  ob_end_clean();
  ob_start();
}

function content_collect(&$out) 
{
  $out .= ob_get_contents();
  ob_end_clean();
  ob_start();
}

function ValidId($id)
{
  return (is_numeric($id) && intval($id) > 0);
}

function InitUserSesssion()
{
  session_start();
  $cookie = null;

  if (isset($_COOKIE[COOKIE_NAME]))
    $cookie = $_COOKIE[COOKIE_NAME];
  
  if ($cookie && $cookie != 'null')
  {
    $session = new Session(NULL, $cookie);
    global $CURRENT_USER;
    $CURRENT_USER = $session->GetSessionUser();
    
    if (!ValidId($CURRENT_USER->id))
    {
//      $createAccountForm = new CreateAccount('createAccountForm', 'Create Account');
//      RegisterModalForm($createAccountForm, '/api/user/CreateAccount.php');
//      $loginForm = new LoginForm('loginForm', 'Log In');
//      RegisterModalForm($loginForm, '/api/user/Login.php');
    }
  }
  else
  {
//    $createAccountForm = new CreateAccount('createAccountForm', 'Create Account');
//    RegisterModalForm($createAccountForm, '/api/user/CreateAccount.php');
//    $loginForm = new LoginForm('loginForm', 'Log In');
//    RegisterModalForm($loginForm, '/api/user/Login.php');
  }
}

/**
 * 
 * @global type $MODAL_FORMS
 * @param FormModal $form
 * @param type $apiUrl
 * @param type $isModal
 */
function RegisterModalForm($form, $apiUrl, $isModal = TRUE)
{
  $form->url = $apiUrl;
  $form->isModal = $isModal;
  global $MODAL_FORMS;
  $MODAL_FORMS[$form->formId] = $form;
}

function PrintRegisteredModalForms()
{
  global $MODAL_FORMS;
  
  foreach ($MODAL_FORMS as $form)
  {
    print $form->Render();
    $script = '<script>$(document).ready(function(){window.' . $form->formId . ' = new ModalForm(\'' . $form->formId . '\')});'
            . '$( "#' . $form->formId . '" ).submit(function( event ) {
    event.preventDefault();
    var pars = window.' . $form->formId . '.collectFormParams();
    window.' . $form->formId . '.showLoadingProgress();
    if (window.' . $form->formId . '.fileUpload.haveFileUpload)
    {
      window.' . $form->formId . '.fileUpload.ajaxData.url = "' . $form->url . '";
      window.' . $form->formId . '.fileUpload.ajaxData.pars = pars;
      window.' . $form->formId . '.fileUpload.ajaxData.fnSuccess = function(response){location.reload();};
      window.' . $form->formId . '.fileUpload.ajaxData.fnError = function(response){ if(response.error) window.' . $form->formId . '.setError(response.error); window.' . $form->formId . '.hideLoadingProgress();};
      window.' . $form->formId . '.fileUpload.data.submit();
    }
    else
      AjaxRequest("' . $form->url . '", pars, function(response){location.reload();}, function(response){ if(response.error) window.' . $form->formId . '.setError(response.error); window.' . $form->formId . '.hideLoadingProgress();});
  });'
            . '</script>';
    print $script;
  }
}

function SendResponse($response = array(), $exit = true, $sendCode = true)
{
  if (!is_array($response))
    $response = array($response);
  if (!array_key_exists('code', $response) && $sendCode)
    $response['code'] = REQUST_SUCCESS;
  
  $jsonText = json_encode($response);
  print $jsonText;
  if ($exit === true)
    exit(0);
}
function SendErrorResponse($message = array(), $code = null)
{
  if (!is_array($message))
  {
    $text = $message;
    $message = array('error' => $text);
  }
  global $RESPONSE_JSON;
  if (!is_numeric($code))
    $code = REQUST_ERROR;
  $RESPONSE_JSON = array('code' => $code, 'message' => $message);

  SendResponse($RESPONSE_JSON, true);
}

function UserExist($nick, $password)
{
  $where = ' u.email=' . ToSqlQuotedString($nick)
         . ' AND u.password = ' . ToSqlQuotedString($password);
  
  $user = User::LoadUsers($where);
  
  if (isset($user[0]))
    $user1 = $user[0];
  else
    $user1 = new User();
  
  return $user1;
}

/**
 * 
 * @param User $user
 * @return type
 */
function LogInUserAndReload($user)
{ 
  $session = new Session($user);
  
  return $session->sessionId;
}

function GetCurrentUser()
{
  global $CURRENT_USER;
  
  return isset($CURRENT_USER->id) ?  $CURRENT_USER : new User();
}

function GetBasePath()
{
  return "/";
}

/**
 * 
 * @param String $path
 */
function GenerateImageAbsolutePath($path)
{
  if (strpos($path, 'http'))
    return $path;
  else
    return SITE_URL . GetBasePath() . $path;
}

function RedirectPage($url)
{
  $url = SITE_URL . GetBasePath() . $url;
  header('Location: ' . $url, true, 302);
  exit(0);
}

function GetDataTablePaginationData($requestData)
{
  $paginationData = array();
  
  $paginationData['sortColumn'] = $requestData['sortColumn'];
  $paginationData['sortDir'] = strtoupper($requestData['sortDir']);
  $paginationData['from'] = $requestData['from'];
  $paginationData['length'] = $requestData['length'];
  
  return $paginationData;
}

function CreateFolder($dir)
{
  if (!file_exists($dir))
    mkdir ($dir, 0775, true);
}

function FormatFileSize($bytes, $precision = 2) 
{ 
  $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

  $bytes = max($bytes, 0); 
  $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
  $pow = min($pow, count($units) - 1); 

  // Uncomment one of the following alternatives
   $bytes /= pow(1024, $pow);
  // $bytes /= (1 << (10 * $pow)); 

  return round($bytes, $precision) . ' ' . $units[$pow]; 
}

function HashString($data)
{
  return md5($data);
}

?>