<?php

function AddCSS($src, $addFingerprint = false)
{
  global $EXTERNAL_CSS;
  global $EXTERNAL_CSS_FINGERPRINTS;
  
  if (!is_array($EXTERNAL_CSS))
    $EXTERNAL_CSS = array();
  
  $cssSheetsRef = &$EXTERNAL_CSS;
  if ($addFingerprint)
    $EXTERNAL_CSS_FINGERPRINTS[$src] = URL_FINGERPRINT;
  
  //verify for not adding second time
  if (in_array($src, $cssSheetsRef))
    return;

  array_push($cssSheetsRef, $src);
}

function AddJS($src, $addFingerprint = false)
{
  global $EXTERNAL_JS;
  global $EXTERNAL_JS_FINGERPRINTS;
  
  if (!is_array($EXTERNAL_JS))
    $EXTERNAL_JS = array();
  
  $jsSheetsRef = &$EXTERNAL_JS;
  if ($addFingerprint)
    $EXTERNAL_JS_FINGERPRINTS[$src] = URL_FINGERPRINT;
  
  //verify for not adding second time
  if (in_array($src, $jsSheetsRef))
    return;

  array_push($jsSheetsRef, $src);
}

function AddScriptJS($script, $addFingerprint = false)
{
  global $BODY_JS;
  
  if (!is_array($BODY_JS))
    $BODY_JS = array();
  
  
  array_push($BODY_JS, $script);
}
