<?php
  function BuildUrl($parts)
  {
    if (!is_array($parts))
      $parts = array ($parts);
    return GetRootFolder() . implode('/', $parts);
  }
  
  function ParseUrlAddres()
  {
    global $URL_PARTS, $URL_ADDRESS, $URL_QUERY;
    $URL_PARTS = array();
    if ($_SERVER['REQUEST_URI'] != '/favicon.ico')
    {
      $url = $_SERVER['REQUEST_URI'];
      
      $i = strpos($url, GetRootFolder());
      if ($i === 0)
        $url = substr($url, $i + strlen(GetRootFolder()));
      
      $i = strpos($url, '?');
      $url_query_tmp = null;
      if ($i !== false) 
      {
        $url_query_tmp = substr($url, $i + 1);
        $url = substr($url, 0, $i);
      }
      
      $parts = explode('/', $url);
      foreach ($parts as $part)
        if (trim($part) != '')
          $URL_PARTS[] = urldecode($part); // urldecode decodes utf8/unicode chars from url

      $URL_ADDRESS = BuildUrl($URL_PARTS);
      $URL_QUERY = $url_query_tmp;
    }
    
  }
  
function GetControllerUrl($controller, $param = null)
{
  global $CONTROLLER_TO_URL;
  if (!array_key_exists($controller, $CONTROLLER_TO_URL))
    return null;
  $url = $CONTROLLER_TO_URL[$controller];
  
  if ($param)
    $url .= "/" . $param;
  
  return '/' . $url;
}
  
  function GetController()
  {
    global $URL_PARTS, $URL_TO_CONTROLLER, $CURRENT_CONTROLLER;
    $controller = null;
    $currentController = "";
    $current_controller_url = "";
    $url = join("/", $URL_PARTS);
    
    //alias here if needed
    foreach ($URL_TO_CONTROLLER as $controller_url => $controller)
    {
      // escape regular expression
      $regex = str_replace("/", "\/", $controller_url);
      if (preg_match("/^{$regex}$/", $url))
      {
        $current_controller_url = $controller_url;
        $currentController = $controller;
        break;
      }
    }
    
    $CURRENT_CONTROLLER = $currentController;
    return $currentController;
  }
  
  function GetControllerPath($controller)
  {
    global $CONTROLLERS;
    if (array_key_exists($controller, $CONTROLLERS))
      return $CONTROLLERS[$controller];
  }
?>
