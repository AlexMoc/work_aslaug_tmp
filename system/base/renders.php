<?php

function RenderTable($id, $headers = array(), $rows = array())
{
  $rowsTag = '';
  foreach ($rows as $key => $value)
  {
    $rowsTag .= '<tr><td>' . $key . '</td>' . '<td>' . $value . '</td></tr>';
  }
  $html = '<table id="'. $id . '">'
            . $rowsTag;
  $html . '</table>';
  
  return $html;
}

function RenderClearItem()
{
  return '<div style="clear: both;"></div>';
}