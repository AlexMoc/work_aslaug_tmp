<?php
require('./system/base/initial-load.php');

$logInForm = new LoginForm('loginForm', "Login", "g-py-15");
RegisterModalForm($logInForm, '/api/user/Login.php', false);

require_once(VIEWS_PATH . 'login.php');
?>
