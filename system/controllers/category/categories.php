<?php
require('./system/base/initial-load.php');
$currentUser = GetCurrentUser();
global $URL_PARTS;
$category = $URL_PARTS[1];

if (!array_key_exists($category, Video::$CATEGORY_TITLES))
{
  print "invalid category";
}

$where = ' v.category=' . ToSqlQuotedString($category)
       . ' AND status!=' . ToSqlQuotedString(Video::STATUS_PRIVATE);
$categoryVideos = Video::LoadVideos($where, null, "ORDER BY v.id DESC");

require_once(VIEWS_PATH . 'category/categories.php');
?>
