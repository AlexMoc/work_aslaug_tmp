<?php
require('./system/base/initial-load.php');

global $URL_PARTS;
$video = new Video($URL_PARTS[1]);
$videoStats = $video->GetStats();
$paginationData = array(
  'from' => 0,
  'length' => VIDEO_ACTIVITIES_FEED_SIZE
);
$where = 'va.video_id=' . ToSqlQuotedString($video->id)
       . ' AND va.status=' . ToSqlQuotedString(VideoActivity::STATUS_NEW)
       . ' AND va.parent_id IS NULL';
$videoActivities = VideoActivity::LoadVideoActivities($where, null, "ORDER BY id DESC", $paginationData);

$videoActivitiesIds = array();
foreach ($videoActivities as $videoActivity)
{
  $videoActivitiesIds[] = $videoActivity->id;
}

if (!ValidId($video->id))
{
  print "invalid video";
}
else
{
  $video->views++;
  $video->Save();
}
$user = User::LoadUser($video->userId);
$recomendedVideos = Video::LoadRecomendedVideos($video, $user);
require_once(VIEWS_PATH . 'video.php');
?>
