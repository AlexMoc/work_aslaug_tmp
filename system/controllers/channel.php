<?php
require('./system/base/initial-load.php');

global $URL_PARTS;
$displayedUser = new User($URL_PARTS[1]);
if (!ValidId($channel->id))
{
  print "invalid channel";
}

$where = ' v.user_id=' . ToSqlQuotedString($displayedUser->id);
$displayedUserVideos = Video::LoadVideos($where, null, "ORDER BY v.date_uploaded DESC");

$latestVideo = array_shift($displayedUserVideos);
$currentUser = GetCurrentUser();
require_once(VIEWS_PATH . 'channel.php');
?>
