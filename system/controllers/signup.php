<?php
require('./system/base/initial-load.php');

$signUpForm = new SignUpForm('signUpForm', "Sign Up", "g-py-15");
RegisterModalForm($signUpForm, '/api/user/CreateAccount.php', false);

require_once(VIEWS_PATH . 'signup.php');
?>
