<?php

/**
 * 
 * @param Video $video
 */
function UpdateVideoStats($video)
{
  if (!ValidId($video->id))
    return;
  $videoStats = $video->GetStats();
  $query = "SELECT COUNT(id) AS cnt FROM likes WHERE liked_entity_id=" . ToSqlQuotedString($video->id)
          . ' AND like_type=' . ToSqlQuotedString(Like::ENTITY_VIDEO)
          . ' AND like_category=' . ToSqlQuotedString(Like::CATEGORY_LIKE);
  $rows = ExecuteQuery($query);
  $videoStats->likesCount = $rows[0]['cnt'];
  
  $query = "SELECT COUNT(id) AS cnt FROM likes WHERE liked_entity_id=" . ToSqlQuotedString($video->id)
          . ' AND like_type=' . ToSqlQuotedString(Like::ENTITY_VIDEO)
          . ' AND like_category=' . ToSqlQuotedString(Like::CATEGORY_DISLIKE);
  $rows = ExecuteQuery($query);
  $videoStats->dislikesCount = $rows[0]['cnt'];
  $videoStats->dateUpdated = FormatSqlDate(time());
  $videoStats->Save();
}

/**
 * 
 * @param VideoActivity $videoActivity
 */
function UpdateCommentStats($videoActivity)
{
  if (!ValidId($videoActivity->id))
    return;

  $query = "SELECT COUNT(id) AS cnt FROM likes WHERE liked_entity_id=" . ToSqlQuotedString($videoActivity->id)
          . ' AND like_type=' . ToSqlQuotedString(Like::ENTITY_COMMENT)
          . ' AND like_category=' . ToSqlQuotedString(Like::CATEGORY_LIKE);
  $rows = ExecuteQuery($query);
  $videoActivity->likesCount = $rows[0]['cnt'];
  
  $query = "SELECT COUNT(id) AS cnt FROM likes WHERE liked_entity_id=" . ToSqlQuotedString($videoActivity->id)
          . ' AND like_type=' . ToSqlQuotedString(Like::ENTITY_COMMENT)
          . ' AND like_category=' . ToSqlQuotedString(Like::CATEGORY_DISLIKE);
  $rows = ExecuteQuery($query);
  $videoActivity->dislikesCount = $rows[0]['cnt'];
  $videoActivity->dateUpdated = FormatSqlDate(time());
  $videoActivity->Save();
}