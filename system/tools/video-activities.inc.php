<?php

/**
 * 
 * @param VideoActivity $videoActivity
 */
function RenderVideoActivity($videoActivity, $renderReplies = false)
{
  $author = new User($videoActivity->userId);
  $currentUser = GetCurrentUser();
  if (!ValidId($videoActivity->id))
    return;
  
  if ($renderReplies)
  {
    $paginationData = array(
      'from' => 0,
      'length' => 2
    );
    $where = 'va.video_id=' . ToSqlQuotedString($videoActivity->videoId)
         . ' AND va.status=' . ToSqlQuotedString(VideoActivity::STATUS_NEW)
         . ' AND va.parent_id=' . ToSqlQuotedString($videoActivity->id); 
    $activitiesReplies = VideoActivity::LoadVideoActivities($where, null, "ORDER BY id DESC", $paginationData);
  }
  
  $authorHtml = '';
  if ($currentUser->id == $author->id)
    $authorHtml = '<a class="u-tags-v1 g-font-size-12 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15" href="#!">Author</a>';
  
  $html = '<div id="va-' . $videoActivity->id . '" class="media g-mb-30 va-board" data-activity-id="' . $videoActivity->id . '">'
          . '<img class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-20" src="../images/goku.jpg" alt="Image Description">'
          . '<div class="media-body g-brd-around g-brd-gray-light-v4 g-pa-30">'
            . '<div class="g-mb-15">'
              . '<h5 class="d-flex justify-content-between align-items-center h5 g-color-gray-dark-v1 mb-0">'
                . '<span class="d-block g-mr-10">' . $author->userName . '</span>'
                . $authorHtml
              . '</h5>'
              . '<span class="g-color-gray-dark-v4 g-font-size-12">' . time_elapsed_string($videoActivity->date) . '</span>'
            . '</div>'
            . '<p>' . $videoActivity->comment . '</p>'
            . '<ul class="list-inline d-sm-flex my-0">'
              . '<li class="list-inline-item g-mr-20">'
                . '<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#" onclick="LikeComment(this, ' . $videoActivity->id . '); return false;">'
                  . '<i class="icon-like g-pos-rel g-top-1 g-mr-3"></i>'
                  . '<span class="likes-count">' . $videoActivity->likesCount . '</span>'
                . '</a>'
              . '</li>'
              . '<li class="list-inline-item g-mr-20">'
                . '<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#" onclick="DislikeComment(this, ' . $videoActivity->id . '); return false;">'
                  . '<i class="icon-dislike g-pos-rel g-top-1 g-mr-3"></i>'
                  . '<span class="dislikes-count">' . $videoActivity->dislikesCount . '</span>'
                . '</a>'
              . '</li>'
              . '<li class="list-inline-item ml-auto">'
                . '<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#" onclick="AddReplyForm(this, ' . $videoActivity->id . '); return false;">'
                  . '<i class="icon-note g-pos-rel g-top-1 g-mr-3"></i>'
                  . 'Reply'
                . '</a>'
              . '</li>'
            . '</ul>'
          . '</div>'
        . '</div>';
  
  if ($renderReplies)
  {
    $html .=  '<div class="activitity-replies-wrapper" data-activity-id="' . $videoActivity->id . '">'
              . '<div class="activity-replies-content">';
    foreach ($activitiesReplies as $activityReply)
    {
      $html .= RenderReplyActivity($activityReply);
    }
    $html .=    '</div>';
    
    if ($paginationData['total'] > ($paginationData['from'] + $paginationData['length']))
      $html .= RenderAnchor ("#", 'Show all replies', 'show-more-replies', array('onclick' => 'VideoActivityPage.loadAllreplies(this); return false;'));
    
    $html .= '</div>';
  }
  
  return $html;
}


/**
 * 
 * @param VideoActivity $videoActivity
 */
function RenderReplyActivity($videoActivity)
{
  $author = new User($videoActivity->userId);
  $currentUser = GetCurrentUser();
  if (!ValidId($videoActivity->id))
    return;
  
  $authorHtml = '';
  if ($currentUser->id == $author->id)
    $authorHtml = '<a class="u-tags-v1 g-font-size-12 g-brd-around g-brd-gray-light-v4 g-bg-primary--hover g-brd-primary--hover g-color-black-opacity-0_8 g-color-white--hover rounded g-py-6 g-px-15" href="#!">Author</a>';
  
  $html = '<div id="va-reply-' . $videoActivity->id . '" class="media g-mb-30 va-reply g-ml-40">'
          . '<img class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-20" src="../images/goku.jpg" alt="Image Description">'
          . '<div class="media-body g-brd-around g-brd-gray-light-v4 g-pa-30">'
            . '<div class="g-mb-15">'
              . '<h5 class="d-flex justify-content-between align-items-center h5 g-color-gray-dark-v1 mb-0">'
                . '<span class="d-block g-mr-10">' . $author->userName . '</span>'
                . $authorHtml
              . '</h5>'
              . '<span class="g-color-gray-dark-v4 g-font-size-12">' . time_elapsed_string($videoActivity->date) . '</span>'
            . '</div>'
            . '<p>' . $videoActivity->comment . '</p>'
            . '<ul class="list-inline d-sm-flex my-0">'
              . '<li class="list-inline-item g-mr-20">'
                . '<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#" onclick="LikeComment(this, ' . $videoActivity->id . '); return false;">'
                  . '<i class="icon-like g-pos-rel g-top-1 g-mr-3"></i>'
                  . '<span class="likes-count">' . $videoActivity->likesCount . '</span>'
                . '</a>'
              . '</li>'
              . '<li class="list-inline-item g-mr-20">'
                . '<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#" onclick="DislikeComment(this, ' . $videoActivity->id . '); return false;">'
                  . '<i class="icon-dislike g-pos-rel g-top-1 g-mr-3"></i>'
                  . '<span class="dislikes-count">' . $videoActivity->dislikesCount . '</span>'
                . '</a>'
              . '</li>'
              . '<li class="list-inline-item ml-auto">'
                . '<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#" onclick="AddReplyForm(this, ' . $videoActivity->id . '); return false;">'
                  . '<i class="icon-note g-pos-rel g-top-1 g-mr-3"></i>'
                  . 'Reply'
                . '</a>'
              . '</li>'
            . '</ul>'
          . '</div>'
        . '</div>';
  return $html;
}