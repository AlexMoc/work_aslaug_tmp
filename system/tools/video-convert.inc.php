<?php

function ConvertToAvi1($src, $dst)
{
  $folder = dirname($dst);
  if (!file_exists($folder))
    CreateFolder($folder);
  
  LogConversionInfoToFile('----------- START -------------');
  LogConversionInfoToFile("Converting $src to $dst");
  $log = str_replace('avi', "txt", $dst);
//  $command = '"' . FFMPEG_PATH . '" -i "' . $src . '" -y -vcodec libx264 -vpre normal -vpre baseline -crf 19 -acodec libmp3lame -ar 44100 -ac 2 "' . $dst . " 1> $log 2>&1";
  $command = FFMPEG_COMMAND . " -i $src -qscale 0 -vcodec mpeg4 -acodec ac3 $dst 1> $log 2>&1";
  //$command = "avconv -i $src -c:a copy -c:v mpeg2video $dst";
  
//  $command = "avconv -i $src -vcodec libx264 -y $dst 1> $log 2>&1";
  LogConversionInfoToFile($command);
  $res = exec($command);
  LogConversionInfoToFile("Response:" . var_export($res, true));
  if (file_exists($dst))
  {
    LogConversionInfoToFile('SUCCESS!!! File created.');
    return true;
  }
  else
  {
    LogConversionInfoToFile('ERROR!!!');
    return false;
  }
  LogConversionInfoToFile('----------- END ----------------');
  return $res != null;
}

/**
 * 
 * @param ConvertJob $convetJob
 * @return boolean
 */
function ConvertToAvi($convetJob)
{
  $src = $convetJob->srcFilePath;
  $dst = $convetJob->dstFilePath;
  $convetJob->commandUsed = FFMPEG_COMMAND;
  $convetJob->convertFrom = pathinfo($src, PATHINFO_EXTENSION);
  $convetJob->convertTo = 'avi';
  $convetJob->type = ConvertJob::TYPE_VIDEO;
  $folder = dirname($dst);
  if (!file_exists($folder))
    CreateFolder($folder);
  
  LogConversionInfoToFile('----------- START -------------');
  LogConversionInfoToFile("Converting $src to $dst");
  $log = str_replace('avi', "txt", $dst);
//  $command = '"' . FFMPEG_PATH . '" -i "' . $src . '" -y -vcodec libx264 -vpre normal -vpre baseline -crf 19 -acodec libmp3lame -ar 44100 -ac 2 "' . $dst . " 1> $log 2>&1";
  $command = FFMPEG_COMMAND . " -i $src -qscale 0 -vcodec mpeg4 -acodec ac3 $dst 1> $log 2>&1";
  //$command = "avconv -i $src -c:a copy -c:v mpeg2video $dst";
  
//  $command = "avconv -i $src -vcodec libx264 -y $dst 1> $log 2>&1";
  $convetJob->endDate = FormatSqlDatetime(time());
  LogConversionInfoToFile($command);
  $res = exec($command);
  LogConversionInfoToFile("Response:" . var_export($res, true));
  if (file_exists($dst))
  {
    $convetJob->status = ConvertJob::STATUS_SUCCESS;
    $convetJob->convertedFileName = pathinfo($convetJob->dstFilePath, PATHINFO_FILENAME);
    $convetJob->convertedFileSize = filesize($convetJob->dstFilePath);
    LogConversionInfoToFile('SUCCESS!!! File created.');
    $result = true;
  }
  else
  {
    $convetJob->status = ConvertJob::STATUS_ERROR;
    LogConversionInfoToFile('ERROR!!!');
    $result = false;
  }

  $convetJob->Save();
//  $convetJob->SetBigId();
  LogConversionInfoToFile('----------- END ----------------');
  return $result;
}