<?php

function RenderAnchor($href, $title, $class, $attrs = array())
{
  return '<a class="' . $class . '" href="' . $href . '" ' . RenderHtmlTagAttributes($attrs) . '>' . $title . '</a>';
}

function RenderButton($href, $title, $class = '', $wrapperClass = '', $attrs = array(), $upperText = '', $subtext = '')
{
  $html = '';
  
  $html = '<div class="btn-wrapper ' . $wrapperClass . '">'
          . '<span>' . $upperText . '</span>'
          . RenderAnchor($href, $title, 'big-btn full-filled ' . $class, $attrs)
          . '<span>' . $subtext . '</span>'
//          . '<a class="green" href="#" onclick="window.createAccountForm.show(); return false;">Create New Account</a>'
        . '</div>';
  
  return $html;
}

function RenderHtmlTagAttributes($attrs = array())
{
  $strAttr = '';
  foreach ($attrs as $key => $value)
  {
    if ($key == 'href')
      continue;
    $strAttr .= ' ' . $key . '="' . htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false) . '"';
  }
  return $strAttr;
}

function RenderDropDownMenu($item = array())
{
  if (count($item) <= 0)
    return;
  $html = '';
  
  $html = '<div class="dropdown">';
          
  if (isset($item['href']))
    $html .= '<button class="dropbtn">' . RenderAnchor ($item['href'], $item['title']) . '</button>';
  else
    $html .= '<button class="dropbtn">' . $item['title'] . '</button>';
  
  $html .= '<div class="dropdown-content">';
      
  if (isset($item['anchors']))
    foreach ($item['anchors'] as $value) 
    {
      $html .= RenderAnchor($value['href'], $value['title'], '', $value);
    }
  
  $html .= '</div>'
   . '</div>';
  
  return $html;
}

