<?php

function PrintPage($content, $options = array())
{
  $isAdminPage = isset($options['isAdmin']) && $options['isAdmin'];
  $pageContentStyle= '';
  if ($isAdminPage)
    $pageContentStyle = 'width: 100%;';
  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    
    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="shortcut icon" href="http://aslaug.byethost11.com/favicon.ico" />
    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat%3A300%2C400%2C500%2C600%2C700%2C900%7COpen+Sans%3A300%2C400%2C500%2C600%2C700%2C800%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik"/>
	
    
    <!--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>-->
    <!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">-->
    <?php
    PrintIncludedCSSFiles();
    PrintIncludedJSFiles();
    ?>
  </head>
  <?php
    global $BODY_JS;
    foreach ($BODY_JS as $value) {
      print $value;
    }
  ?>
  <body>
    <main>
      <!-- Header -->
      <header id="js-header" class="u-header u-header--static--lg u-header--show-hide--lg u-header--change-appearance--lg" data-header-fix-moment="500" data-header-fix-effect="slide">
        <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10" data-header-fix-moment-exclude="g-bg-white g-py-10" data-header-fix-moment-classes="g-bg-white-opacity-0_9 u-shadow-v18 g-py-0">
          <nav class="navbar navbar-expand-lg">
            <div class="container">
              <!-- Responsive Toggle Button -->
              <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->
              <!-- Logo -->
              <a href="<?php print GetControllerUrl('index'); ?>" class="navbar-brand">
                <img class="header_logo" src="/images/logo_black.png" alt="Aslaug's logo">
              </a>
              <!-- End Logo -->

              <!-- Navigation -->
              <div class="js-mega-menu collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--l g-font-size-14" id="navBar">
                <ul class="navbar-nav text-uppercase g-font-weight-600 ml-auto g-lg-5">
                  <li class="nav-item hs-has-sub-menu g-mx-20--lg">
                    <a href="category_page.html" class="nav-link g-px-0" id="nav-link-1" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu-1">
                      Category
                    </a>
                    <!-- Submenu -->
                    <ul class="hs-sub-menu list-unstyled g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-min-width-200 g-mt-20 g-mt-10--lg--scrolling" id="nav-submenu-1" aria-labelledby="nav-link-1">
                      <?php 
                        $html = '';
                        foreach (Video::$CATEGORY_TITLES as $key => $value)
                        {
                          $html .= '<li class="dropdown-item">'
                                  . '<a class="nav-link g-px-0" href="' . GetControllerUrl('category', $key) . '">' . $value . '</a>'
                                . '</li>';
                        }
                        
                        print $html;
                      ?>
                    </li>
                  </ul>
                    <!-- End Submenu -->
                  </li>
                  <li class="nav-item g-mx-20--lg">
                    <form id="searchform-1">
                      <div class="input-group g-brd-primary--focus">
                        <input class="form-control rounded-0 u-form-control" type="search" placeholder="Search...">
                        <div class="input-group-addon p-0">
                          <button class="btn rounded-0  u-btn-white btn-md g-font-size-14 g-px-18" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>
                <ul class="navbar-nav text-uppercase g-font-weight-600 ml-auto g-lg-5 u-main-nav-v5">
                  <li class="nav-item g-mx-20--lg ">
                    <a href="<?php print GetControllerUrl('upload');?>" class="nav-link px-0"><i class="icon-cloud-upload"></i> Upload</a>
                  </li>
                  <li class="nav-item g-mx-20--lg ">
                    <a href="<?php print GetControllerUrl('login'); ?>" class="nav-link px-0"><i class="icon-login"></i> Login</a>
                  </li>
                  <li class="nav-item g-mx-20--lg ">
                    <!--<a href="signup_page.html" class="nav-link px-0"><i class="icon-user-follow"></i> signup</a>-->
                    <?php
                    if (ValidId(GetCurrentUser()->id))
                    {
                      ?>
                    <a href="#" onclick="LogoutUser(); return false;" class="nav-link px-0"><i class="icon-user-follow"></i> logout</a>
                      <?php
                    }
                    else
                    {
                      ?>
                      <a href="<?php print GetControllerUrl('signup') ?>" class="nav-link px-0"><i class="icon-user-follow"></i> signup</a>
                      <?php
                    }
                    ?>
                  </li>
                </ul>
              </div>
              <!-- End Navigation -->
            </div>
          </nav>
        </div>
      </header>
      <!-- End Header --><!-- Blog Background Overlay Blocks -->
      <?php
        print $content; 
  //      PrintFacebookInitScript();
      ?>
      <!-- Copyright Footer -->
      <footer class="g-bg-darkgray-radialgradient-circle g-color-white-opacity-0_8 g-py-20">
          <div class="container">
            <div class="row">
                <div class="col-md-8 text-center text-md-left g-mb-15 g-mb-0--md">
                  <div class="d-lg-flex">
                      <a href="http://hebastudio.com" target="_blank" style="color: white; text-decoration-line: none;"><small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2018 © Aslaug</small></a>
                      <ul class="u-list-inline">
                        <li class="list-inline-item">
                            <a href="#!">Privacy Policy</a>
                        </li>
                        <li class="list-inline-item">
                            <span>|</span>
                        </li>
                        <li class="list-inline-item">
                            <a href="#!">Terms of Use</a>
                        </li>
                      </ul>
                  </div>
                </div>

                <div class="col-md-4 align-self-center">
                  <ul class="list-inline text-center text-md-right mb-0">
                      <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                        <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-facebook"></i>
                        </a>
                      </li>
                      <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                        <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-skype"></i>
                        </a>
                      </li>
                      <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                        <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-linkedin"></i>
                        </a>
                      </li>
                      <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Pinterest">
                        <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-pinterest"></i>
                        </a>
                      </li>
                      <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                        <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-twitter"></i>
                        </a>
                      </li>
                      <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Dribbble">
                        <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-dribbble"></i>
                        </a>
                      </li>
                  </ul>
                </div>
            </div>
          </div>
      </footer>
      <!-- End Copyright Footer -->
      <a class="js-go-to u-go-to-v1" href="#" data-type="fixed" data-position='{
        "bottom": 15,
        "right": 15
      }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
         <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
    </main>
  </body>
</html>
<?php
}

function PrintIncludedCSSFiles()
{
  global $EXTERNAL_CSS;
  global $EXTERNAL_CSS_FINGERPRINTS;
  
  // first print external
  for ($i = 0, $count = count($EXTERNAL_CSS); $i < $count; $i++)
  {
    $src = $EXTERNAL_CSS[$i];
    $fingerprint = @$EXTERNAL_CSS_FINGERPRINTS[$src];
    print '<link type="text/css" href="' . $src . $fingerprint . '" rel="stylesheet" />' . "\n";
  }
}

function PrintFacebookInitScript()
{
  print '
<div id="fb-root"></div>
<script type="text/javascript">
window.fbAsyncInit = function()
{
  FB.init({
    appId: "956187467850329",
    cookie: true,
    xfbml: true,
    oauth: true,
    frictionlessRequests: false
  });

  try {
  ' . '' . '
  }
  catch(e){if (console && console.error) console.error(e)}
};
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));
</script>
';
}

function PrintIncludedJSFiles()
{
  global $EXTERNAL_JS;
  global $EXTERNAL_JS_FINGERPRINTS;
  
  // first print external
  for ($i = 0, $count = count($EXTERNAL_JS); $i < $count; $i++)
  {
    $src = $EXTERNAL_JS[$i];
    $fingerprint = @$EXTERNAL_JS_FINGERPRINTS[$src];
    print '<script type="text/javascript" src="' . $src . $fingerprint . '"></script>' . "\n";
  }
}