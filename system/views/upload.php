<?php
start_content();
?>
<script type="text/javascript">
$(document).on('ready', function () {
  $("#videoFormSubmit").click(function(e){
    e.preventDefault();
    var pars = {
      title: $("#videoUploadForm input#title").val(),
      description: $("#videoUploadForm textarea#description").val(),
      category: $("#category option:selected" ).val(),
      thumbUrl: $("input.image" ).val(),
      videoUrl: $("input.video" ).val()
    };
    AjaxRequest('api/UploadVideo.php', pars, function(response){
      
    },
    function(response){
      alert(response.error);
    });
  });
  
  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  // initialization of carousel
  $.HSCore.components.HSCarousel.init('.js-carousel');

  // initialization of masonry
  $('.masonry-grid').imagesLoaded().then(function () {
    $('.masonry-grid').masonry({
      columnWidth: '.masonry-grid-sizer',
      itemSelector: '.masonry-grid-item',
      percentPosition: true
    });
  });
  $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
      $.HSCore.helpers.HSFocusState.init();
  // initialization of popups
  $.HSCore.components.HSPopup.init('.js-fancybox');
  $(".video-upload-wrapper div.u-file-attach-v3").html('<i class="icon-cloud-upload g-font-size-16 g-pos-rel g-top-2 g-mr-5 g-font-size-36"></i><span class="js-value g-font-size-20">Select file to upload</span>');
});

$(window).on('load', function () {
  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  $.HSCore.helpers.HSHamburgers.init('.hamburger');

  // initialization of HSMegaMenu component
  $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 991
  });
  
  
});
</script>
<?php
content_collect($script);
AddScriptJS($script);


/* -------- CONTENT START -------- */
start_content();
?>
<!-- Blog Minimal Blocks -->
<div class="container g-pt-100 g-pb-20">
	<div class="row justify-content-between">
		<!-- General Forms -->
		<form id="videoUploadForm" class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30 col-lg-12">
			<!-- Text Input -->
			<div class="form-group g-mb-20 col-lg-6">
				<label class="g-mb-10" for="inputGroup1_1">Video Title</label>
				<input id="title" class="form-control form-control-md rounded-0" type="email" placeholder="Enter Video's name">
			</div>
			<!-- End Text Input -->

			 <!-- Textarea Resizable -->
			<div class="form-group g-mb-20 col-lg-12">
				<label class="g-mb-10" for="inputGroup2_2">Description</label>
				<textarea id="description" class="form-control form-control-md rounded-0" rows="3" placeholder="Description area"></textarea>
			</div>
			<!-- End Textarea Resizable -->

			<!-- Textarea Expandable -->
			<div class="form-group g-mb-20 col-lg-6">
				<label class="mr-sm-3 mb-3 mb-lg-0 g-mb-10" for="inlineFormCustomSelectPref">Category</label>
          <?php
          $htmlSelectElement = '<option selected="" value="">Choose...</option>';
          foreach(Video::$CATEGORY_TITLES as $key => $value)
            $htmlSelectElement .= '<option value="' . $key . '">' . $value . '</option>';
          ?>
			    <select id="category" class="custom-select mb-3 col-lg-12" >
				    <?php print $htmlSelectElement;?>
			    </select>
			</div>
			<!-- End Textarea Expandable -->

			<hr class="g-brd-gray-light-v4 g-mx-minus-30">

			<h4 class="h6 g-font-weight-700 g-mb-20 col-lg-12">Add Thumbnail</h4>

			 <!-- Advanced File Input -->
			<div class="form-group mb-0 col-lg-12">
				<label class="g-mb-10">Thumbnail image will be used as video logo image</label>
				<input class="js-file-attachment" accept='image/*' type="file" name="fileAttachment2[]">
			</div>
			<!-- End Advanced File Input -->

			<hr class="g-brd-gray-light-v4 g-mx-minus-30">

			<h4 class="h6 g-font-weight-700 g-mb-20 col-lg-12">Add Video</h4>

			<!-- Plain File Input -->
			<div class="form-group mb-0 text-center col-lg-12 video-upload-wrapper">
				<p>Only MP4 video file</p>
        <input class="js-file-attachment" accept='video/*' type="file" name="fileAttachment2[]">
				<label class="u-file-attach-v2 g-color-gray-dark-v5 mb-0">
					<!--<input id="fileAttachment" accept='video/*' name="file-attachment" type="file">-->
					<!--<i class="icon-cloud-upload g-font-size-16 g-pos-rel g-top-2 g-mr-5 g-font-size-36"></i>-->
					<!--<span class="js-value g-font-size-20">Select file to upload</span>-->
				</label>
        
			</div>
			<!-- End Plain File Input -->
			
			<hr class="g-brd-gray-light-v4 g-mx-minus-30">

			<div class="text-center">
				<a href="#!" id="videoFormSubmit" class="btn btn-md u-btn-primary g-mr-10 g-mb-15">Submit</a>
			</div>
		</form>
		<!-- End General Forms -->
	</div>
</div>
<!-- End Blog Minimal Blocks -->
<?php
content_collect($CONTENT);
PrintPage($CONTENT);
/* -------- CONTENT END -------- */
?>
