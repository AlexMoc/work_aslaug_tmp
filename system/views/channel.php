<?php
start_content();
?>
<script type="text/javascript">
$(document).on('ready', function () {  
  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  // initialization of carousel
  $.HSCore.components.HSCarousel.init('.js-carousel');

  // initialization of masonry
  $('.masonry-grid').imagesLoaded().then(function () {
    $('.masonry-grid').masonry({
      columnWidth: '.masonry-grid-sizer',
      itemSelector: '.masonry-grid-item',
      percentPosition: true
    });
  });
  $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
      $.HSCore.helpers.HSFocusState.init();
  // initialization of popups
  $.HSCore.components.HSPopup.init('.js-fancybox');
  $(".video-upload-wrapper div.u-file-attach-v3").html('<i class="icon-cloud-upload g-font-size-16 g-pos-rel g-top-2 g-mr-5 g-font-size-36"></i><span class="js-value g-font-size-20">Select file to upload</span>');
});

$(window).on('load', function () {
  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  $.HSCore.helpers.HSHamburgers.init('.hamburger');

  // initialization of HSMegaMenu component
  $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 991
  });
  
  
});
</script>
<?php
content_collect($script);
AddScriptJS($script);


/* -------- CONTENT START -------- */
start_content();
?>
<!-- Blog Minimal Blocks -->
<div class="container g-pt-100 g-pb-20">
	<div class="row justify-content-between">
		<div class="media g-mb-25 col-lg-6">
			<img class="d-flex g-width-40 g-height-40 rounded-circle mr-2" src="../images/vegeta.jpg" alt="Image Description">
			<div class="media-body">
        <h4 class="h6 g-color-primary mb-0"><a href="<?php print GetControllerUrl('channel', $displayedUser->id); ?>"><?php print $displayedUser->userName; ?></a></h4>
				<span class="d-block g-color-gray-dark-v4 g-font-size-12"><?php print date('F d Y', strtotime($latestVideo->dateUploaded))?> - 5 min read</span>
			</div>
		</div>
		<div class="g-mb-25 col-lg-6 text-right">
			<a class="btn u-btn-outline-primary g-font-size-11 g-rounded-25" href="#!">Follow</a>
		</div>
		<div class="col-lg-6 g-mb-80">
			<!-- Blog Minimal Blocks -->
			<article class="g-mb-100">
				<div class="g-mb-30">

					<!-- HTML5 Example -->
					<div class="embed-responsive embed-responsive-16by9 g-mb-60">
            <video preload="auto" controls="controls" poster="<?php print $latestVideo->GetThumbUrl(); ?>"> <!-- thumbnail -->
							<source src="http://aslaug.byethost11.com/upload/video-bg.webm" type="video/webm;">
								<source src="<?php print GenerateImageAbsolutePath($latestVideo->videoUrl); ?>" type="video/mp4;">
						</video>
					</div>
					<!-- End HTML5 Example -->

          <h2 class="h4 g-color-black g-font-weight-600 mb-3"><a class="u-link-v5 g-color-black g-color-primary--hover" href="#" onclick="return false;"><?php print $latestVideo->title; ?></a></h2>
				</div>
			</article>
			<!-- End Blog Minimal Blocks -->

			<!-- carsual slider -->
			<section id="team" class="g-theme-bg-blue-dark-v1">
				<div class="container">
					<!-- Team Block -->
					<div class="g-mx-minus-9--sm">
						<div class="js-carousel"
								 data-slides-show="2"
								 data-infinite="true"
								 data-arrows-classes="u-arrow-v1 g-pos-abs g-top-35x g-width-45 g-height-45 g-color-white g-theme-bg-blue-dark-v2 g-bg-primary--hover g-rounded-50x g-transition-0_2 g-transition--ease-in"
								 data-arrow-left-classes="fa fa-chevron-left g-left-0 g-ml-minus-15"
								 data-arrow-right-classes="fa fa-chevron-right g-right-0 g-mr-minus-15"
								 data-responsive='[{
									 "breakpoint": 1200,
									 "settings": {
										 "slidesToShow": 2,
										 "slidesToScroll": 2
									 }
								 }, {
									 "breakpoint": 768,
									 "settings": {
										 "slidesToShow": 2,
										 "slidesToScroll": 2
									 }
								 }, {
									 "breakpoint": 576,
									 "settings": {
										 "slidesToShow": 1,
										 "slidesToScroll": 1
									 }
								 }]'>
              
              <?php
              $usersVideosHtml = '';
              foreach ($displayedUserVideos as $displayedUserVideo)
              {
                $usersVideosHtml .= '<div class="js-slide g-px-5">'
                                    . '<figure class="text-center">'
                                      . '<img class="w-100 g-mb-35" src="' . $displayedUserVideo->GetThumbUrl() . '" alt="Image Description" />'
                                      . '<div class="text-uppercase g-mb-20">'
                                        . '<h4 class="g-letter-spacing-2 g-font-size-11 g-theme-color-gray-dark-v3 g-mb-10">' . $displayedUserVideo->title . '</h4>'
                                        . '<h2 class="g-font-weight-600 g-font-size-18 g-color-white">James Novel</h2>'
                                      . '</div>'
                                    . '</figure>'
                                  . '</div>';
              }
              
              print $usersVideosHtml;
              ?>

						</div>
					</div>
					<!-- End Team Block -->
				</div>
			</section>
			<!-- End carsual slider -->
		</div>

		<div class="col-lg-4 g-brd-left--lg g-brd-gray-light-v4 ">
			<h3 class="h5 g-color-black g-font-weight-600 mb-4">Playlist</h3>
			<div class="row">
				<div class="col-lg-6 g-mb-30">
					<!-- Article -->
					<article class="u-block-hover">
						<figure class="u-bg-overlay g-bg-black-gradient-opacity-v1--after">
							<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="../images/whis.jpg" alt="Image Description">
						</figure>

						<span class="g-pos-abs g-top-20 g-left-20">
							<a class="btn btn-sm u-btn-black rounded-0" href="#!">History</a>
						</span>

						<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
							<div class="media">
								<div class="media-body align-self-center g-color-white">
									<p class="mb-0 g-font-size-14">18</p></i>
								</div>
							</div>
						</div>
					</article>
					<!-- End Article -->
				</div>

				<div class="col-lg-6 g-mb-30">
					<!-- Article -->
					<article class="u-block-hover">
						<figure class="u-bg-overlay g-bg-black-gradient-opacity-v1--after">
							<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="../images/vegeta.jpg" alt="Image Description">
						</figure>

						<span class="g-pos-abs g-top-20 g-left-20">
							<a class="btn btn-sm u-btn-black rounded-0" href="#!">Education</a>
						</span>

						<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
							<div class="media">
								<div class="media-body align-self-center g-color-white">
									<p class="mb-0 g-font-size-14">37</p></i>
								</div>
							</div>
						</div>
					</article>
					<!-- End Article -->
				</div>

				<div class="col-lg-6 g-mb-30">
					<!-- Article -->
					<article class="u-block-hover">
						<figure class="u-bg-overlay g-bg-black-gradient-opacity-v1--after">
							<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="../images/goku.jpg" alt="Image Description">
						</figure>

						<span class="g-pos-abs g-top-20 g-left-20">
							<a class="btn btn-sm u-btn-black rounded-0" href="#!">Art</a>
						</span>

						<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
							<div class="media">
								<div class="media-body align-self-center g-color-white">
									<p class="mb-0 g-font-size-14">28</p></i>
								</div>
							</div>
						</div>
					</article>
					<!-- End Article -->
				</div>

				<div class="col-lg-6 g-mb-30">
					<!-- Article -->
					<article class="u-block-hover">
						<figure class="u-bg-overlay g-bg-black-gradient-opacity-v1--after">
							<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="../images/beerus.jpg" alt="Image Description">
						</figure>

						<span class="g-pos-abs g-top-20 g-left-20">
							<a class="btn btn-sm u-btn-black rounded-0" href="#!">Science</a>
						</span>

						<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
							<div class="media">
								<div class="media-body align-self-center g-color-white">
									<p class="mb-0 g-font-size-14">45</p></i>
								</div>
							</div>
						</div>
					</article>
					<!-- End Article -->
				</div>

				<div class="col-lg-6 g-mb-30">
					<!-- Article -->
					<article class="u-block-hover">
						<figure class="u-bg-overlay g-bg-black-gradient-opacity-v1--after">
							<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="../images/kaioshin.jpg" alt="Image Description">
						</figure>

						<span class="g-pos-abs g-top-20 g-left-20">
							<a class="btn btn-sm u-btn-black rounded-0" href="#!">Wisdom</a>
						</span>

						<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
							<div class="media">
								<div class="media-body align-self-center g-color-white">
									<p class="mb-0 g-font-size-14">10</p></i>
								</div>
							</div>
						</div>
					</article>
					<!-- End Article -->
				</div>
			</div>

			<h3 class="h5 g-color-black g-font-weight-600 mb-4">News</h3>
			<div class="row">
				<!-- Textarea Input with Left Appended Icon -->
				<div class="form-group g-mb-20 col-lg-12">
					<div class="input-group g-brd-primary--focus g-pb-10">
						<div class="input-group-prepend">
							<span class="input-group-text rounded-0 g-bg-white g-color-gray-light-v1"><i class="icon-user"></i> &nbsp</span>
						</div>
						<textarea class="form-control form-control-md g-resize-none rounded-0" rows="4" placeholder="Update info for people subscribed to the channel"></textarea>
					</div>
					<div class="text-right"><a href="#!" class="btn u-btn-primary g-mr-10 g-mb-15">Submit</a></div>
				</div>
				<!-- End Textarea Input with Left Appended Icon -->
			</div>

			<h3 class="h5 g-color-black g-font-weight-600 mb-4">Comments</h3>
			<div class="row">
				<div class="media g-mb-30">
					<img class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-15" src="../images/whis.jpg" alt="Image Description">
					<div class="media-body g-brd-around g-brd-gray-light-v4">
						<div class="g-mb-15">
							<h5 class="h5 g-color-gray-dark-v1 mb-0">Whis</h5>
							<span class="g-color-gray-dark-v4 g-font-size-12">2 days ago</span>
						</div>

						<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue
							felis in faucibus ras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>

						<ul class="list-inline d-sm-flex my-0">
							<li class="list-inline-item g-mr-20">
								<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
									<i class="icon-like g-pos-rel g-top-1 g-mr-3"></i>
									178
								</a>
							</li>
							<li class="list-inline-item g-mr-20">
								<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
									<i class="icon-dislike g-pos-rel g-top-1 g-mr-3"></i>
									19
								</a>
							</li>
							<li class="list-inline-item ml-auto">
								<a class="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
									<i class="icon-note g-pos-rel g-top-1 g-mr-3"></i>
									Reply
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- Textarea Input with Left Appended Icon -->
				<div class="form-group g-mb-20 col-lg-12">
					<div class="input-group g-brd-primary--focus g-pb-10">
						<div class="input-group-prepend">
							<span class="input-group-text rounded-0 g-bg-white g-color-gray-light-v1"><i class="icon-user"></i> &nbsp</span>
						</div>
						<textarea class="form-control form-control-md g-resize-none rounded-0" rows="4" placeholder="Cras Ullamcorper Nibh"></textarea>
					</div>
					<div class="text-right"><a href="#!" class="btn u-btn-primary g-mr-10 g-mb-15">Submit</a></div>
				</div>
				<!-- End Textarea Input with Left Appended Icon -->
			</div>
		</div>
		<div class="col-lg-2 g-brd-left--lg g-brd-gray-light-v4 ">

			<h3 class="h5 g-color-black g-font-weight-600 mb-4">Information</h3>
			<div class="row g-pl-10">
				<p class="g-font-weight-600">Channel Name</p>
				I'm 40 years old. I fight. I sleep. 
				<p class="g-font-weight-600 g-pt-10">Vegeta</p>
				<!-- Textarea Input with Left Appended Icon -->
				Hello, I am Vegeta, the prince Vegeta. Elite Warrior.
				<!-- End Textarea Input with Left Appended Icon -->
				<div><p class="g-font-weight-600 g-pt-10">Email:</p>vegeta@mail.com</div>
				<div class="g-font-weight-600 g-pt-10">
					<a class="u-icon-v1 g-color-facebook g-color-facebook--hover g-mr-15 g-mb-20" href="#!">
						<i class="fa fa-facebook"></i>
					</a>
					<a class="u-icon-v1 g-color-twitter g-color-twitter--hover g-mr-15 g-mb-20" href="#!">
						<i class="fa fa-twitter"></i>
					</a>
					<a class="u-icon-v1 g-color-pinterest g-color-pinterest--hover g-mr-15 g-mb-20" href="#!">
						<i class="fa fa-pinterest-p"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
		<!-- End Blog Minimal Blocks -->
<?php
content_collect($CONTENT);
PrintPage($CONTENT);
/* -------- CONTENT END -------- */
?>
