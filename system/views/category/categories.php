<?php
start_content();
?>
<script type="text/javascript">
$(document).on('ready', function () {  
  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  // initialization of carousel
  $.HSCore.components.HSCarousel.init('.js-carousel');

  // initialization of masonry
  $('.masonry-grid').imagesLoaded().then(function () {
    $('.masonry-grid').masonry({
      columnWidth: '.masonry-grid-sizer',
      itemSelector: '.masonry-grid-item',
      percentPosition: true
    });
  });
  $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
      $.HSCore.helpers.HSFocusState.init();
  // initialization of popups
  $.HSCore.components.HSPopup.init('.js-fancybox');
  $(".video-upload-wrapper div.u-file-attach-v3").html('<i class="icon-cloud-upload g-font-size-16 g-pos-rel g-top-2 g-mr-5 g-font-size-36"></i><span class="js-value g-font-size-20">Select file to upload</span>');
});

$(window).on('load', function () {
  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  $.HSCore.helpers.HSHamburgers.init('.hamburger');

  // initialization of HSMegaMenu component
  $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 991
  });
  
  
});
</script>
<?php
content_collect($script);
AddScriptJS($script);


/* -------- CONTENT START -------- */
start_content();
?>
<!-- Blog Minimal Blocks -->
<div class="container g-pt-70 g-pb-20">
	<div class="row justify-content-between">
		<div class="media g-mb-25 col-lg-6">
			<h1 class="h3 text-uppercase g-color-primary g-font-weight-600"><?php print $category;?></h1>
		</div>
		<div class="g-mb-25 col-lg-6 text-right">
			<a class="btn u-btn-outline-primary g-font-size-13 g-rounded-25" href="#!">Follow</a>
		</div>

		<div class="col-lg-10 g-mb-80">
			<div class="u-heading-v3-1 g-mb-40">
			  	<h2 class="h3 u-heading-v3__title">Top Videos</h2>
			</div>
			<section id="team" class="g-theme-bg-blue-dark-v1 g-mb-40">
				<div class="container">
					<!-- Team Block -->
					<div class="g-mx-minus-9--sm">
						<div class="js-carousel"
								 data-slides-show="4"
								 data-infinite="true"
								 data-arrows-classes="u-arrow-v1 g-pos-abs g-top-35x g-width-45 g-height-45 g-color-white g-theme-bg-blue-dark-v2 g-bg-primary--hover g-rounded-50x g-transition-0_2 g-transition--ease-in"
								 data-arrow-left-classes="fa fa-chevron-left g-left-0 g-ml-minus-15"
								 data-arrow-right-classes="fa fa-chevron-right g-right-0 g-mr-minus-15"
								 data-responsive='[{
									 "breakpoint": 1200,
									 "settings": {
										 "slidesToShow": 4,
										 "slidesToScroll": 4
									 }
								 }, {
									 "breakpoint": 768,
									 "settings": {
										 "slidesToShow": 4,
										 "slidesToScroll": 4
									 }
								 }, {
									 "breakpoint": 576,
									 "settings": {
										 "slidesToShow": 1,
										 "slidesToScroll": 1
									 }
								 }]'>
              <?php
              $categoryVideosHtml = '';
              foreach ($categoryVideos as $categoryVideo)
              {
                $categoryVideosHtml .= '<div class="js-slide g-px-5">'
                                      . '<article class="u-block-hover">'
                                        . '<figure class="u-bg-overlay g-bg-black-opacity-0_3--after">'
                                          . '<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="' . $categoryVideo->GetThumbUrl() . '" alt="Image Description" />'
                                        . '</figure>'
                                        . '<div class="media g-pos-abs g-top-20 g-left-20">'
                                          . '<div class="d-flex mr-3">'
                                            . '<img class="g-width-40 g-height-40 rounded-circle" src="../images/bqQw89E.png" alt="Image Description" />'
                                          . '</div>'
                                          . '<div class="media-body align-self-center g-color-white">'
                                            . '<p class="mb-0">John Wallmart</p>'
                                          . '</div>'
                                        . '</div>'
                                        . '<a href="' . GetControllerUrl('video', $categoryVideo->id) . '">'
                                          . '<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered">'
                                            . '<i class="fa fa-play g-left-2"></i>'
                                          . '</span>'
                                        . '</a>'
                                        . '<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">'
                                          . '<small class="g-color-white"> ' . date('F d, Y', strtotime($categoryVideo->dateUploaded)) . ' </small>'
                                          . '<hr class="g-my-10 g-opacity-0_5">'
                                          . '<ul class="u-list-inline g-font-size-12 g-color-white">'
                                            . '<li class="list-inline-item">'
                                              . '<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> ' . $categoryVideo->views
                                            . '</li>'
                                            . '<li class="list-inline-item">/</li>'
                                            . '<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li>'
                                            . '<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li>'
                                          . '</ul>'
                                        . '</div>'
                                      . '</article>'
                                      . '<h4 class="h5 g-font-weight-600 g-mt-10">'
                                        . '<a class="g-color-black g-color-primary--hover v-title" href="' . GetControllerUrl('video', $categoryVideo->id) . '">' . $categoryVideo->title . '</a>'
                                      . '</h4>'
                                    . '</div>';
              }
              
              print $categoryVideosHtml;
              ?>					
						</div>
					</div>
					<!-- End Team Block -->
				</div>
			</section>

			<div class="u-heading-v3-1 g-mb-40">
			  	<h2 class="h3 u-heading-v3__title">Playlist</h2>
			</div>
			<section id="team" class="g-theme-bg-blue-dark-v1 g-mb-40">
				<div class="container">
					<!-- Team Block -->
					<div class="g-mx-minus-9--sm">
						<div class="js-carousel"
								 data-slides-show="4"
								 data-infinite="true"
								 data-arrows-classes="u-arrow-v1 g-pos-abs g-top-35x g-width-45 g-height-45 g-color-white g-theme-bg-blue-dark-v2 g-bg-primary--hover g-rounded-50x g-transition-0_2 g-transition--ease-in"
								 data-arrow-left-classes="fa fa-chevron-left g-left-0 g-ml-minus-15"
								 data-arrow-right-classes="fa fa-chevron-right g-right-0 g-mr-minus-15"
								 data-responsive='[{
									 "breakpoint": 1200,
									 "settings": {
										 "slidesToShow": 4,
										 "slidesToScroll": 4
									 }
								 }, {
									 "breakpoint": 768,
									 "settings": {
										 "slidesToShow": 4,
										 "slidesToScroll": 4
									 }
								 }, {
									 "breakpoint": 576,
									 "settings": {
										 "slidesToShow": 1,
										 "slidesToScroll": 1
									 }
								 }]'>
							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="PD1356938962t07.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">John Wallmart</p> 
										</div> 
									</div> 
									<a href="video_page.html">
										<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
											<i class="fa fa-play g-left-2"></i> 
										</span>
									</a>
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article -->
								<h4 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="video_page.html">Computer Science</a> 
								</h4> 
								<!-- End Figure -->
							</div>

							<div class="js-slide g-px-5">
								<!-- Article --> 
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img3.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img9.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">John Wallmart</p> 
										</div> 
									</div> 
									<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
										<i class="fa fa-play g-left-2"></i> 
									</span> 
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article --> 
								<h3 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="#!">Florida Beaches - Must be visited places in the USA</a> 
								</h3> 
							</div>

							<div class="js-slide g-px-5">
								<!-- Article --> 
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img8.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img4.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">Alisha Bower</p> 
										</div> 
									</div> 
									<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
										<i class="fa fa-play g-left-2"></i> 
									</span> 
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20"> 
										<small class="g-color-white"> July 15, 2016 </small>  
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> <i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 264 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 52 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 26 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article --> 
								<h3 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="#!">Holidays are coming, prepare your gifts to your...</a> 
								</h3>
							</div>

							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img9.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img5.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">Linda Reyes</p> 
										</div> 
									</div> 
									<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
										<i class="fa fa-play g-left-2"></i> 
									</span> 
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20"> 
										<small class="g-color-white"> July 15, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> <i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 349 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 152 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 96 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article --> 
								<h3 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="#!">Best 10 breakfast receipts for your awesome morning day</a> 
								</h3> 
								<!-- End Figure -->
							</div>

							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img7.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img7.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">John Wallmart</p> 
										</div> 
									</div> 
									<a href="http://aslaug.byethost11.com/video">
										<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
											<i class="fa fa-play g-left-2"></i> 
										</span>
									</a>
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article -->
								<h4 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="http://aslaug.byethost11.com/video">Must be visited places in the USA - Florida Beaches</a> 
								</h4> 
								<!-- End Figure -->
							</div>

							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img7.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img7.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">John Wallmart</p> 
										</div> 
									</div> 
									<a href="http://aslaug.byethost11.com/video">
										<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
											<i class="fa fa-play g-left-2"></i> 
										</span>
									</a>
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article -->
								<h4 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="http://aslaug.byethost11.com/video">Must be visited places in the USA - Florida Beaches</a> 
								</h4> 
								<!-- End Figure -->
							</div>							
						</div>
					</div>
					<!-- End Team Block -->
				</div>
			</section>

			<div class="u-heading-v3-1 g-mb-40">
			  	<h2 class="h3 u-heading-v3__title">Live Videos</h2>
			</div>
			<section id="team" class="g-theme-bg-blue-dark-v1 g-mb-40">
				<div class="container">
					<!-- Team Block -->
					<div class="g-mx-minus-9--sm">
						<div class="js-carousel"
								 data-slides-show="4"
								 data-infinite="true"
								 data-arrows-classes="u-arrow-v1 g-pos-abs g-top-35x g-width-45 g-height-45 g-color-white g-theme-bg-blue-dark-v2 g-bg-primary--hover g-rounded-50x g-transition-0_2 g-transition--ease-in"
								 data-arrow-left-classes="fa fa-chevron-left g-left-0 g-ml-minus-15"
								 data-arrow-right-classes="fa fa-chevron-right g-right-0 g-mr-minus-15"
								 data-responsive='[{
									 "breakpoint": 1200,
									 "settings": {
										 "slidesToShow": 4,
										 "slidesToScroll": 4
									 }
								 }, {
									 "breakpoint": 768,
									 "settings": {
										 "slidesToShow": 4,
										 "slidesToScroll": 4
									 }
								 }, {
									 "breakpoint": 576,
									 "settings": {
										 "slidesToShow": 1,
										 "slidesToScroll": 1
									 }
								 }]'>
							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="bqQw89E.png" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img7.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">FairyChess</p> 
										</div> 
									</div> 
									<a href="video_page.html">
										<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
											<i class="fa fa-play g-left-2"></i> 
										</span>
									</a>
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article -->
								<h4 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="http://aslaug.byethost11.com/video">Learn Blockchain LIVE !</a> 
								</h4> 
								<!-- End Figure -->
							</div>

							<div class="js-slide g-px-5">
								<!-- Article --> 
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img3.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img9.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">John Wallmart</p> 
										</div> 
									</div> 
									<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
										<i class="fa fa-play g-left-2"></i> 
									</span> 
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article --> 
								<h3 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="#!">Florida Beaches - Must be visited places in the USA</a> 
								</h3> 
							</div>

							<div class="js-slide g-px-5">
								<!-- Article --> 
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img8.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img4.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">Alisha Bower</p> 
										</div> 
									</div> 
									<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
										<i class="fa fa-play g-left-2"></i> 
									</span> 
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20"> 
										<small class="g-color-white"> July 15, 2016 </small>  
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> <i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 264 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 52 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 26 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article --> 
								<h3 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="#!">Holidays are coming, prepare your gifts to your...</a> 
								</h3>
							</div>

							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img9.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img5.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">Linda Reyes</p> 
										</div> 
									</div> 
									<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
										<i class="fa fa-play g-left-2"></i> 
									</span> 
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20"> 
										<small class="g-color-white"> July 15, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> <i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 349 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 152 </li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 96 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article --> 
								<h3 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="#!">Best 10 breakfast receipts for your awesome morning day</a> 
								</h3> 
								<!-- End Figure -->
							</div>

							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img7.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img7.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">John Wallmart</p> 
										</div> 
									</div> 
									<a href="http://aslaug.byethost11.com/video">
										<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
											<i class="fa fa-play g-left-2"></i> 
										</span>
									</a>
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article -->
								<h4 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="http://aslaug.byethost11.com/video">Must be visited places in the USA - Florida Beaches</a> 
								</h4> 
								<!-- End Figure -->
							</div>

							<div class="js-slide g-px-5">
								<!-- Figure -->
								<article class="u-block-hover"> 
									<figure class="u-bg-overlay g-bg-black-opacity-0_3--after"> 
										<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="http://aslaug.byethost11.com/assets/img/500x650/img7.jpg" alt="Image Description"> 
									</figure> 
									<div class="media g-pos-abs g-top-20 g-left-20"> 
										<div class="d-flex mr-3"> 
											<img class="g-width-40 g-height-40 rounded-circle" src="http://aslaug.byethost11.com/assets/img/100x100/img7.jpg" alt="Image Description"> 
										</div> 
										<div class="media-body align-self-center g-color-white"> 
											<p class="mb-0">John Wallmart</p> 
										</div> 
									</div> 
									<a href="http://aslaug.byethost11.com/video">
										<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered"> 
											<i class="fa fa-play g-left-2"></i> 
										</span>
									</a>
									<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">  
										<small class="g-color-white"> July 02, 2016 </small> 
										<hr class="g-my-10 g-opacity-0_5"> 
										<ul class="u-list-inline g-font-size-12 g-color-white"> 
											<li class="list-inline-item"> 
												<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> 132 
											</li> 
											<li class="list-inline-item">/</li> 
											<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li> 
											<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li> 
										</ul> 
									</div> 
								</article> 
								<!-- End Article -->
								<h4 class="h5 g-font-weight-600 g-mt-10"> 
									<a class="g-color-black g-color-primary--hover v-title" href="http://aslaug.byethost11.com/video">Must be visited places in the USA - Florida Beaches</a> 
								</h4> 
								<!-- End Figure -->
							</div>							
						</div>
					</div>
					<!-- End Team Block -->
				</div>
			</section>
		</div>

		<!-- Left Sidebar -->
		<div class="col-lg-2 g-brd-left--lg g-brd-gray-light-v4 g-mb-80">
			<div class="g-pl-20--lg">
				<div id="stickyblock-start" class="js-sticky-block g-sticky-block--lg g-pt-50" data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">
					<!-- Publications -->
					<div class="g-mb-50">
						<ul class="list-unstyled g-font-size-13 mb-0">
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
							<li>
								<article class="media g-mb-35">
									<a href="#">
										<img class="g-width-40 g-height-40 rounded-circle" src="bqQw89E.png" alt="Image Description"> 
									</a>
									<div class="media-body">
										<h4 class="h6 g-color-black g-font-weight-600">Htmlstream</h4>
										<p class="g-color-gray-dark-v4">Anas</p>
									</div>
								</article>
							</li>
						</ul>
					</div>
					<!-- End Publications -->
				</div>
			</div>
		</div>
		<!-- End Left Sidebar -->
	</div>
	
</div>
<!-- End Blog Minimal Blocks -->
<?php
content_collect($CONTENT);
PrintPage($CONTENT);
/* -------- CONTENT END -------- */
?>
