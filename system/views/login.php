<?php
start_content();
?>
<script type="text/javascript">
$(document).on('ready', function () {
  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  // initialization of carousel
  $.HSCore.components.HSCarousel.init('.js-carousel');

  // initialization of masonry
  $('.masonry-grid').imagesLoaded().then(function () {
    $('.masonry-grid').masonry({
      columnWidth: '.masonry-grid-sizer',
      itemSelector: '.masonry-grid-item',
      percentPosition: true
    });
  });
  $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
      $.HSCore.helpers.HSFocusState.init();
  // initialization of popups
  $.HSCore.components.HSPopup.init('.js-fancybox');
});

$(window).on('load', function () {
  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  $.HSCore.helpers.HSHamburgers.init('.hamburger');

  // initialization of HSMegaMenu component
  $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 991
  });
});
</script>
<?php
content_collect($script);
AddScriptJS($script);


/* -------- CONTENT START -------- */
start_content();
?>
<!-- Login -->
<section class="clearfix">
  <div class="row no-gutters align-items-center">
    <div class="col-lg-6">
      <!-- Promo Block - Slider -->
      <div class="js-carousel" data-autoplay="true" data-infinite="true" data-fade="true" data-speed="5000">
        <div class="js-slide g-bg-size-cover g-min-height-100vh" style="background-image: url(images/bqQw89E.png);" data-calc-target="#js-header"></div>

        <div class="js-slide g-bg-size-cover g-min-height-100vh" style="background-image: url(images/PD1356938962t07.jpg);" data-calc-target="#js-header"></div>
      </div>
      <!-- End Promo Block - Slider -->
    </div>

    <div class="col-lg-6">
      <div class="g-pa-50 g-mx-70--xl">
        <!-- Form -->
        <?php $logInForm->PrintForm(); ?>
        <!-- End Form -->
      </div>
    </div>
  </div>
</section>
		<!-- End Login -->
<?php
content_collect($CONTENT);
PrintPage($CONTENT);
/* -------- CONTENT END -------- */
?>
