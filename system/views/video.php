<?php
start_content();
?>
<script type="text/javascript">
$(document).on('ready', function () {  
  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  // initialization of carousel
  $.HSCore.components.HSCarousel.init('.js-carousel');

  // initialization of masonry
  $('.masonry-grid').imagesLoaded().then(function () {
    $('.masonry-grid').masonry({
      columnWidth: '.masonry-grid-sizer',
      itemSelector: '.masonry-grid-item',
      percentPosition: true
    });
  });
  $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
      $.HSCore.helpers.HSFocusState.init();
  // initialization of popups
  $.HSCore.components.HSPopup.init('.js-fancybox');
  window.currentVideo = {
    id: <?php print $video->id; ?>
  };
  $(".video-upload-wrapper div.u-file-attach-v3").html('<i class="icon-cloud-upload g-font-size-16 g-pos-rel g-top-2 g-mr-5 g-font-size-36"></i><span class="js-value g-font-size-20">Select file to upload</span>');
  $(".btn-submit-comment").click(function(e){
    e.preventDefault();
    var _this = this;
    var pars = {
      comment: $("textarea.comment").val(),
      videoId: <?php print $video->id?>
    };
    AjaxRequest('/api/PostComment.php', pars, function(response){
      $(response.videoActivity).hide().prependTo("#viedo-activities").fadeIn("slow");
      $(_this).closest(".form-group").find("textarea").val("");
    },
    function(response){
      alert(response.error);
    });
  });
});

$(window).on('load', function () {
  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  $.HSCore.helpers.HSHamburgers.init('.hamburger');

  // initialization of HSMegaMenu component
  $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 991
  });
  
  
});
</script>
<?php
content_collect($script);
AddScriptJS($script);
AddJS('js/video-activity.js');


/* -------- CONTENT START -------- */
start_content();
?>
<!-- Blog Minimal Blocks -->
<div class="container g-pt-100 g-pb-20">
  <div class="row justify-content-between">
    <div class="col-lg-8 g-mb-80">
      <div class="g-pr-20--lg">
        <!-- Blog Minimal Blocks -->
        <article class="g-mb-100">
          <div class="g-mb-30">
            <div class="media g-mb-25">
              <img class="d-flex g-width-40 g-height-40 rounded-circle mr-2" src="../images/6419431g.jpg" alt="Image Description">
              <div class="media-body">
                <h4 class="h6 g-color-primary mb-0"><a href="<?php print GetControllerUrl('channel', $user->id); ?>"><?php print $user->userName; ?></a></h4>
                <span class="d-block g-color-gray-dark-v4 g-font-size-12"><?php print date("F d Y", strtotime($video->dateUploaded)) . ' - ' . time_elapsed_string($video->dateUploaded); ?></span>
              </div>
            </div>

            <!-- HTML5 Example -->
            <div class="embed-responsive embed-responsive-16by9 g-mb-60">
              <video preload="auto" controls="controls" poster="<?php print $video->GetThumbUrl(); ?>">
                <source src="http://aslaug.byethost11.com/upload/video-bg.webm" type="video/webm;">
                <source src="<?php print GenerateImageAbsolutePath($video->videoUrl); ?>" type="video/mp4;">
              </video>
            </div>
            <!-- End HTML5 Example -->

            <h2 class="h4 g-color-black g-font-weight-600 mb-3"><a class="u-link-v5 g-color-black g-color-primary--hover" href="#" onclick="return false;"><?php print $video->title; ?></a></h2>
            <ul class="d-flex justify-content-end list-inline g-brd-y g-brd-gray-light-v3 g-font-size-13 g-py-13 mb-0">
              <li class="list-inline-item mr-auto">
                <i class="icon-eye g-color-primary g-font-size-14 mr-1"></i>
                <font class="g-font-size-14"><?php print $video->views; ?></font> views
              </li>
              <li class="list-inline-item mr-4">
                <a class="d-inline-block g-color-gray-dark-v4 g-text-underline--none--hover" onclick="LikeVideo(this, <?php print $video->id ?>); return false;" href="#"><i class="g-color-primary g-font-size-14 u-line-icon-pro mr-1 icon-like"></i></a>
                <span class="likes-count"><?php print $videoStats->likesCount; ?></span>
              </li>
              <li class="list-inline-item">
                <a class="d-inline-block g-color-gray-dark-v4 g-text-underline--none--hover" onclick="DislikeVideo(this, <?php print $video->id ?>); return false;"  href="#"><i class="align-middle g-color-primary g-font-size-14 mr-1 icon-dislike"></i></a>
                <span class="dislikes-count"><?php print $videoStats->dislikesCount; ?></span>
              </li>
            </ul>
            <p class="g-color-gray-dark-v4 g-line-height-1_8"><?php print $video->description;?></p>
            <a class="g-font-size-13" href="#!">Read more...</a> <!-- to change -->
          </div>
        </article>
        <!-- End Blog Minimal Blocks -->

        <div class="form-group g-mb-20 col-lg-12">
          <div class="input-group g-brd-primary--focus g-pb-10">
            <div class="input-group-prepend">
              <span class="input-group-text rounded-0 g-bg-white g-color-gray-light-v1"><i class="icon-user"></i> &nbsp</span>
            </div>
            <textarea class="form-control form-control-md g-resize-none rounded-0 comment" rows="4" placeholder="Add a comment"></textarea>
          </div>
          <div class="text-right"><a href="#" onclick="return false;" class="btn u-btn-primary g-mr-10 g-mb-15 btn-submit-comment">Submit</a></div>
        </div>
        <!-- Blog Minimal Blocks -->
        <article id="viedo-activities" class="g-mb-100" data-video-id="<?php print $video->id; ?>">
          <?php
          require_once(TOOLS_PATH . 'video-activities.inc.php');
          foreach ($videoActivities as $videoActivity)
          {
            print RenderVideoActivity($videoActivity, true);
          }
          
          ?>
        </article>
        <?php
        if ($paginationData['total'] > VIDEO_ACTIVITIES_FEED_SIZE)
            print RenderAnchor('#', "Load more activities", '', array('onclick' => 'VideoActivityPage.loadMoreActivities(this); return false;'));
        ?>
        <!-- End Blog Minimal Blocks -->
      </div>
    </div>

    <div class="col-lg-4 g-brd-left--lg g-brd-gray-light-v4 g-mb-80">
      <div class="g-pl-20--lg">

        <div id="stickyblock-start" class="js-sticky-block g-sticky-block--lg g-pt-50" data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">
          <!-- Publications -->
          <div class="g-mb-50">
            <h3 class="h5 g-color-black g-font-weight-600 mb-4">Recommendation</h3>
            <?php
            $recomendedVideosHtml = '';
            foreach ($recomendedVideos as $recomendedVideo)
            {
              $videoUser = User::LoadUser($recomendedVideo->id);
              $recomendedVideosHtml .=  '<li>'
                                        . '<article class="media g-mb-35">'
                                          . '<a href="' . GetControllerUrl('video', $recomendedVideo->id) . '">'
                                            . '<img class="d-flex g-width-120 g-height-90 mr-3" src="' . $recomendedVideo->GetThumbUrl() . '" alt="Image Description"/>'
                                          . '</a>'
                                          . '<div class="media-body">'
                                            . '<h4 class="h6 g-color-black g-font-weight-600">' . $recomendedVideo->title . '</h4>'
                                            . '<p class="g-color-gray-dark-v4">' . $videoUser->userName . '</p>'
                                            . '<font class="g-color-primary g-font-size-14">' . $recomendedVideo->views . '</font> views'
                                          . '</div>'
                                        . '</article>'
                                      . '</li>';
            }
            ?>
            <ul class="list-unstyled g-font-size-13 mb-0">
              <?php print $recomendedVideosHtml; ?>
            </ul>
          </div>
          <!-- End Publications -->
        </div>
      </div>
    </div>
  </div>
</div>
		<!-- End Blog Minimal Blocks -->
<?php
content_collect($CONTENT);
PrintPage($CONTENT);
/* -------- CONTENT END -------- */
?>
