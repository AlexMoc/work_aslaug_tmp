<?php
start_content();
?>
<script type="text/javascript">
$(document).on('ready', function () {
  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  // initialization of carousel
  $.HSCore.components.HSCarousel.init('.js-carousel');

  // initialization of masonry
  $('.masonry-grid').imagesLoaded().then(function () {
    $('.masonry-grid').masonry({
      columnWidth: '.masonry-grid-sizer',
      itemSelector: '.masonry-grid-item',
      percentPosition: true
    });
  });
  $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
      $.HSCore.helpers.HSFocusState.init();
  // initialization of popups
  $.HSCore.components.HSPopup.init('.js-fancybox');
});

$(window).on('load', function () {
  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  $.HSCore.helpers.HSHamburgers.init('.hamburger');

  // initialization of HSMegaMenu component
  $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 991
  });
});
</script>
<?php
content_collect($script);
AddScriptJS($script);

$html = '';
foreach ($videos as $video)
{
  $user = User::LoadUser($video->userId);
  $html .= '<div class="col-lg-3 g-mb-60">'
          . '<article class="u-block-hover">'
            . '<figure class="u-bg-overlay g-bg-black-opacity-0_3--after">'
              . '<img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="' . $video->GetThumbUrl() . '" alt="Image Description">'
            . '</figure>'
            . '<div class="media g-pos-abs g-top-20 g-left-20">'
              . '<div class="d-flex mr-3">'
                . '<img class="g-width-40 g-height-40 rounded-circle" src="images/6419431g.jpg" alt="Image Description">'
              . '</div>'
              . '<div class="media-body align-self-center g-color-white">'
                . '<p class="mb-0">' . $user->userName . '</p>'
              . '</div>'
            . '</div>'
            . '<a href="' . GetControllerUrl('video', $video->id) . '">'
              . '<span class="u-icon-v3 g-bg-black-opacity-0_5 g-bg-black--hover g-color-white rounded-circle g-cursor-pointer g-absolute-centered">'
                . '<i class="fa fa-play g-left-2"></i>'
              . '</span>'
            . '</a>'
            . '<div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">'
              . '<small class="g-color-white"> ' . date('F d, Y', strtotime($video->dateUploaded)) . ' </small>'
              . '<hr class="g-my-10 g-opacity-0_5">'
              . '<ul class="u-list-inline g-font-size-12 g-color-white">'
                . '<li class="list-inline-item">'
                  . '<i class="icon-eye g-pos-rel g-top-1 g-mr-2"></i> ' . $video->views . ' '
                . '</li>'
                . '<li class="list-inline-item">/</li>'
                . '<li class="list-inline-item"> <i class="icon-speech g-pos-rel g-top-1 g-mr-2"></i> 23 </li>'
                . '<li class="list-inline-item">/</li> <li class="list-inline-item"> <i class="icon-share g-pos-rel g-top-1 g-mr-2"></i> 12 </li>'
              . '</ul>'
            . '</div>'
          . '</article>'
          . '<h3 class="h5 g-font-weight-600 g-mt-10"> '
            . '<a class="g-color-black g-color-primary--hover v-title" href="' . GetControllerUrl('video', $video->id) . '">' . $video->title . '</a>'
          . '</h3>'
        . '</div>';
}

/* -------- CONTENT START -------- */
start_content();
?>
<div class="container g-pt-70 g-pb-20">
	<div class="u-heading-v3-1 g-mb-40">
	  	<h2 class="h3 u-heading-v3__title">Trending Today</h2>
	</div>
	<div class="shortcode-html">
		<div class="row">
      <?php print $html;?>
		</div>
	</div>
</div>
<?php
content_collect($CONTENT);
PrintPage($CONTENT);
/* -------- CONTENT END -------- */
?>
