<?php
  if ($_SERVER['REQUEST_URI'] == '/favicon.ico')
    return;
  
  require('./system/base/initial-load.php');
  
  ParseUrlAddres();
  
  global $CONTROLLER;
  $CONTROLLER = GetController();
  
 
//  AddCSS('css/forms.css', TRUE);
  //<!-- CSS Global Compulsory -->
  AddCSS('css/bootstrap.min.css', TRUE);
  AddCSS('css/offcanvas.css', TRUE);
  //<!-- CSS Global Icons -->
  AddCSS('css/font-awesome.min.css', TRUE);
  AddCSS('css/simple-line-icons.css', TRUE);
  AddCSS('css/style.css', TRUE);
  AddCSS('css/stylepro.css', TRUE);
  AddCSS('css/stylehs.css', TRUE);
  AddCSS('css/dzsparallaxer.css', TRUE);
  AddCSS('css/scroller.css', TRUE);
  AddCSS('css/plugin.css', TRUE);
  AddCSS('css/animate.css', TRUE);
  AddCSS('css/hamburgers.min.css', TRUE);
  AddCSS('css/hs.megamenu.css', TRUE);
  AddCSS('css/slick.css', TRUE);
  AddCSS('css/jquery.fancybox.css', TRUE);
  //<!-- CSS Unify -->
  AddCSS('css/unify-core.css', TRUE);
  AddCSS('css/unify-components.css', TRUE);
  AddCSS('css/unify-globals.css', TRUE);
  AddCSS('css/jquery-ui.min.css', TRUE);
  //<!-- CSS Customization -->
  AddCSS('css/custom.css', TRUE);
  AddCSS('css/style1.css', TRUE);
//  AddCSS('https://cdn.datatables.net/t/dt/dt-1.10.11/datatables.min.css', TRUE);
//  AddCSS('css/font-awesome.css', TRUE);
//  AddJS('js/jquery-file-upload/jquery-fileupload-combined.js', TRUE);
//  AddJS('https://cdn.datatables.net/t/dt/dt-1.10.11/datatables.min.js', TRUE);
//  AddJS('js/jquery-data-table/jquery-data-table.js', TRUE);
  //<!-- JS Global Compulsory -->
  AddJS('js/jquery.min.js', TRUE);
  AddJS('js/jquery-migrate.min.js', TRUE);
  AddJS('js/popper.min.js', TRUE);
  AddJS('js/bootstrap.min.js', TRUE);
  AddJS('js/offcanvas.js', TRUE);
  AddJS('js/jquery-cookie/jquery.cookie.js', TRUE);
  AddJS('js/global.js', TRUE);
  
  //<!-- JS Implementing Plugins -->
  AddJS('js/hs.megamenu.js', TRUE);
  AddJS('js/dzsparallaxer.js', TRUE);
  AddJS('js/scroller.js', TRUE);
  AddJS('js/plugin.js', TRUE);
  AddJS('js/masonry.pkgd.min.js', TRUE);
  AddJS('js/imagesloaded.pkgd.min.js', TRUE);
  AddJS('js/slick.js', TRUE);
  AddJS('js/jquery.fancybox.min.js', TRUE);
  AddJS('js/hs.core.js', TRUE);
  AddJS('js/hs.header.js', TRUE);
  AddJS('js/hs.hamburgers.js', TRUE);
  AddJS('js/hs.popup.js', TRUE);
  AddJS('js/hs.carousel.js', TRUE);
  AddJS('js/hs.go-to.js', TRUE);
//  AddJS('js/custom.js', TRUE);
  AddJS('js/jquery.filer.min.js', TRUE);
  AddJS('js/hs.focus-state.js', TRUE);
  AddJS('js/hs.file-attachement.js', TRUE);
  AddJS('js/forms.js', TRUE);
  
  $script = '<script>window.SITE_URL = "' . SITE_URL . '";'
          . 'window.SITE_REQUEST_PROTOCOL = "' . SITE_REQUEST_URL . '";</script>';
  AddScriptJS($script);
  
  if (empty($CONTROLLER))
  {
//    PrintPageNotFound();
//    exit();
  }
  
  $controllerPath = GetControllerPath($CONTROLLER);
  if (!$controllerPath)
  {
//    PrintPageNotFound();
//    exit();
  }
  
  if (strlen($controllerPath) > 0)
  {
    require_once($controllerPath);
  }
  
?>